#pragma once
#include <Windows.h>

static const long MAX_DEVICE = 32;
static const long MAX_BUTTON = 32;
static const long MAX_AXIS = 8;
static const long MAX_HAT = 4;
static const long MAX_BALL = 4;
static const long MAX_EVENT = 32;
static const long MAX_TOUCHPAD = 4; // No idea of the actual maximum
static const long MAX_FINGER = 8; // No idea of the actual maximum
static const long MAX_SENSOR = 4; // No idea of the actual maximum
static const long MAX_SDLPLAYER = 8; // No idea of the actual maximum

struct SensorData
{
	float x;
	float y;
	float z;
};

struct FingerData
{
	bool state;
	float x;
	float y;
	float pressure;
};

struct TouchpadData
{
	Uint8 num_fingers;
	FingerData finger[MAX_FINGER];
};

struct SDL_JoystickData
{
	SDL_Joystick* joystick;
	SDL_GameController* controller;
	SDL_Haptic* haptic;
	bool connected;
	Sint8 joy_id;
	Sint16 axis[MAX_AXIS];
	Sint16 axis_initial[MAX_AXIS];
	Uint8 held_buttons[MAX_BUTTON];
	Uint8 held_buttons_last[MAX_BUTTON];
	Uint8 hat[MAX_HAT];
	int ball_x[MAX_BALL];
	int ball_y[MAX_BALL];
	Sint8 currentheld[MAX_BUTTON];
	Sint8 lastpressed;
	Sint8 lastreleased;
	Uint8 num_buttons;
	Uint8 num_axes;
	Uint8 num_hats;
	Uint8 num_balls;
	Uint8 num_touchpads;
	wchar_t name[512];
	SDL_GUID guid;
	wchar_t guid_string[33];
	TouchpadData touchpads[MAX_TOUCHPAD];
	SDL_JoystickPowerLevel battery;
	SensorData accel;
	SensorData accel_left;
	SensorData accel_right;
	SensorData gyro;
	SensorData gyro_left;
	SensorData gyro_right;
	int which; // Device ID used in the SDL_JOYDEVICEADDED event
	bool is_controller; // When a device is opened, this bool stores the result of SDL_IsGameController
	SDL_GameControllerType type_controller; // This is stored to make it possible to retrieve even when GameController is disabled
};

struct GlobalData
{
	bool SDLInitState;
	SDL_JoystickData SDL_Data[MAX_DEVICE];
	bool DisableGameController;
	long NumDevices;
	SDL_Event SDL_Events[MAX_EVENT];
};

extern GlobalData GlobalPdata;