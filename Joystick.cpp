#include "common.h"
#include "SDL.h"
#include "GlobalData.h"

GlobalData GlobalPdata;

// Convert wchar_t* to char*, used for pathnames
// Stolen from https://gist.github.com/xebecnan/6d070c93fb69f40c3673
char* WcharToChar(const wchar_t* src, size_t src_length, size_t* out_length)
{
	if (!src)
		return NULL;
	if (src_length == 0)
		src_length = wcslen(src);
	int length = WideCharToMultiByte(CP_UTF8, 0, src, src_length, 0, 0, NULL, NULL);
	char* output_buffer = (char*)malloc((length + 1) * sizeof(char));
	if (output_buffer)
	{
		WideCharToMultiByte(CP_UTF8, 0, src, src_length, output_buffer, length, NULL, NULL);
		output_buffer[length] = '\0';
	}
	if (out_length)
		*out_length = length;
	return output_buffer;
}

// Update input for a device
void UpdateInput(int i)
{
	if (!GlobalPdata.SDL_Data[i].connected)
		return;
	// Axes
	for (int a = 0; a < GlobalPdata.SDL_Data[i].num_axes; a++)
	{
		if (GlobalPdata.SDL_Data[i].controller != nullptr)
		{
			GlobalPdata.SDL_Data[i].axis[a] = SDL_GameControllerGetAxis(GlobalPdata.SDL_Data[i].controller, (SDL_GameControllerAxis)a);
		}
		else
			GlobalPdata.SDL_Data[i].axis[a] = SDL_JoystickGetAxis(GlobalPdata.SDL_Data[i].joystick, a);
	}
	// Buttons
	for (int b = 0; b < GlobalPdata.SDL_Data[i].num_buttons; b++)
	{
		// Buttons pressed
		if (GlobalPdata.SDL_Data[i].controller != nullptr)
			GlobalPdata.SDL_Data[i].held_buttons[b] = SDL_GameControllerGetButton(GlobalPdata.SDL_Data[i].controller, (SDL_GameControllerButton)b);
		else
			GlobalPdata.SDL_Data[i].held_buttons[b] = SDL_JoystickGetButton(GlobalPdata.SDL_Data[i].joystick, b);
		
	}
	// Hats
	for (int h = 0; h < GlobalPdata.SDL_Data[i].num_hats; h++)
	{
		GlobalPdata.SDL_Data[i].hat[h] = SDL_JoystickGetHat(GlobalPdata.SDL_Data[i].joystick, h);
	}
	// Balls
	for (int l = 0; l < GlobalPdata.SDL_Data[i].num_balls; l++)
	{
		SDL_JoystickGetBall(GlobalPdata.SDL_Data[i].joystick, l, &GlobalPdata.SDL_Data[i].ball_x[l], &GlobalPdata.SDL_Data[i].ball_y[l]);
	}
}

// Reset all data associated with a device
void ResetJoystick(int joy)
{
	GlobalPdata.SDL_Data[joy].connected = false;
	GlobalPdata.SDL_Data[joy].haptic = nullptr;
	GlobalPdata.SDL_Data[joy].joystick = nullptr;
	GlobalPdata.SDL_Data[joy].controller = nullptr;
	GlobalPdata.SDL_Data[joy].is_controller = false;
	GlobalPdata.SDL_Data[joy].joy_id = -1;
	GlobalPdata.SDL_Data[joy].lastpressed = -1;
	GlobalPdata.SDL_Data[joy].lastreleased = -1;
	GlobalPdata.SDL_Data[joy].num_axes = 0;
	GlobalPdata.SDL_Data[joy].num_buttons = 0;
	GlobalPdata.SDL_Data[joy].num_balls = 0;
	GlobalPdata.SDL_Data[joy].num_hats = 0;
	GlobalPdata.SDL_Data[joy].type_controller = SDL_CONTROLLER_TYPE_UNKNOWN;
	wsprintf(GlobalPdata.SDL_Data[joy].name, L"");
	GlobalPdata.SDL_Data[joy].guid = { 0 };
	wsprintf(GlobalPdata.SDL_Data[joy].guid_string, L"");
	// Reset battery status
	GlobalPdata.SDL_Data[joy].battery = SDL_JOYSTICK_POWER_EMPTY;
	// Reset touchpads and fingers
	GlobalPdata.SDL_Data[joy].num_touchpads = 0;
	for (int i = 0; i < MAX_TOUCHPAD; i++)
	{
		GlobalPdata.SDL_Data[joy].touchpads[i].num_fingers = 0;
		for (int f = 0; f < MAX_FINGER; f++)
		{
			GlobalPdata.SDL_Data[joy].touchpads[i].finger[f].state = false;
			GlobalPdata.SDL_Data[joy].touchpads[i].finger[f].x = 0.0f;
			GlobalPdata.SDL_Data[joy].touchpads[i].finger[f].y = 0.0f;
			GlobalPdata.SDL_Data[joy].touchpads[i].finger[f].pressure = 0.0f;
		}
	}
	// Reset axes
	for (int a = 0; a < MAX_AXIS; a++)
	{
		GlobalPdata.SDL_Data[joy].axis[a] = 0;
		GlobalPdata.SDL_Data[joy].axis_initial[a] = 0;
	}
	// Reset buttons
	for (int h = 0; h < MAX_BUTTON; h++)
	{
		GlobalPdata.SDL_Data[joy].currentheld[h] = -1;
		GlobalPdata.SDL_Data[joy].held_buttons[h] = false;
		GlobalPdata.SDL_Data[joy].held_buttons_last[h] = false;
	}
	// Reset hats and balls
	for (int h = 0; h < MAX_HAT; h++)
	{
		GlobalPdata.SDL_Data[joy].hat[h] = 0;
	}
	for (int b = 0; b < MAX_BALL; b++)
	{
		GlobalPdata.SDL_Data[joy].ball_x[b] = 0;
		GlobalPdata.SDL_Data[joy].ball_y[b] = 0;
	}
	// Reset sensors
	GlobalPdata.SDL_Data[joy].accel.x = 0;
	GlobalPdata.SDL_Data[joy].accel.y = 0;
	GlobalPdata.SDL_Data[joy].accel.z = 0;
	GlobalPdata.SDL_Data[joy].accel_left.x = 0;
	GlobalPdata.SDL_Data[joy].accel_left.y = 0;
	GlobalPdata.SDL_Data[joy].accel_left.z = 0;
	GlobalPdata.SDL_Data[joy].accel_right.x = 0;
	GlobalPdata.SDL_Data[joy].accel_right.y = 0;
	GlobalPdata.SDL_Data[joy].accel_right.z = 0;
	GlobalPdata.SDL_Data[joy].gyro.x = 0;
	GlobalPdata.SDL_Data[joy].gyro.y = 0;
	GlobalPdata.SDL_Data[joy].gyro.z = 0;
	GlobalPdata.SDL_Data[joy].gyro_left.x = 0;
	GlobalPdata.SDL_Data[joy].gyro_left.y = 0;
	GlobalPdata.SDL_Data[joy].gyro_left.z = 0;
	GlobalPdata.SDL_Data[joy].gyro_right.x = 0;
	GlobalPdata.SDL_Data[joy].gyro_right.y = 0;
	GlobalPdata.SDL_Data[joy].gyro_right.z = 0;
}

// Close the Joystick/GameController interface and reset the values
void CloseJoystick(int joy)
{
	if (GlobalPdata.SDL_Data[joy].haptic != nullptr)
	{
		SDL_HapticClose(GlobalPdata.SDL_Data[joy].haptic);
		GlobalPdata.SDL_Data[joy].haptic = nullptr;
	}
	if (GlobalPdata.SDL_Data[joy].controller != nullptr)
	{
		SDL_GameControllerClose(GlobalPdata.SDL_Data[joy].controller);
		GlobalPdata.SDL_Data[joy].controller = nullptr;
	}
	if (GlobalPdata.SDL_Data[joy].joystick != nullptr)
	{
		SDL_JoystickClose(GlobalPdata.SDL_Data[joy].joystick);
		GlobalPdata.SDL_Data[joy].joystick = nullptr;
	}
	ResetJoystick(joy);
	GlobalPdata.SDL_Data[joy].which = -1;
}

// Open a Joystick/GameController device
void OpenJoystick(int joy, int which, bool disablecontroller)
{
	// 'which' can only be used when a joystick is being connected! It's reset right afterwards.
	bool controller = false;
	const char* cname;
	// Close if already open
	if (GlobalPdata.SDL_Data[joy].connected)
		CloseJoystick(joy);
	// Close if the same device ID is already opened
	for (int j = 0; j < MAX_DEVICE; j++)
	{
		if (GlobalPdata.SDL_Data[j].connected && GlobalPdata.SDL_Data[j].which == which)
		{
			CloseJoystick(j);
			// If it's already open, use the same device ID for the extension
			joy = j;
		}
	}
	// Set the "is known GameController" variable
	GlobalPdata.SDL_Data[joy].is_controller = SDL_IsGameController(which);
	GlobalPdata.SDL_Data[joy].type_controller = SDL_GameControllerTypeForIndex(which);
	if (GlobalPdata.SDL_Data[joy].is_controller && !disablecontroller)
	{
		// Try to open as GameController
		GlobalPdata.SDL_Data[joy].controller = SDL_GameControllerOpen(which);
		GlobalPdata.SDL_Data[joy].joystick = SDL_GameControllerGetJoystick(GlobalPdata.SDL_Data[joy].controller);
		cname = SDL_GameControllerName(GlobalPdata.SDL_Data[joy].controller);
		if (cname != NULL)
			mbstowcs(GlobalPdata.SDL_Data[joy].name, cname, 512);
		controller = true;
		SDL_GameControllerUpdate();
	}
	// Otherwise open as Joystick
	else
	{
		GlobalPdata.SDL_Data[joy].joystick = SDL_JoystickOpen(which);
		cname = SDL_JoystickName(GlobalPdata.SDL_Data[joy].joystick);
		if (cname != NULL)
			mbstowcs(GlobalPdata.SDL_Data[joy].name, cname, 512);
		SDL_JoystickUpdate();
	}
	// Disconnect on failure
	if (GlobalPdata.SDL_Data[joy].joystick == nullptr)
	{
		ResetJoystick(joy);
		return;
	}
	// Set device GUID
	char guid_char[33];
	GlobalPdata.SDL_Data[joy].guid = SDL_JoystickGetGUID(GlobalPdata.SDL_Data[joy].joystick);
	SDL_JoystickGetGUIDString(GlobalPdata.SDL_Data[joy].guid, guid_char, 33);
	mbstowcs(GlobalPdata.SDL_Data[joy].guid_string, guid_char, 33);
	// Set numbers of axes, buttons etc.
	GlobalPdata.SDL_Data[joy].joy_id = SDL_JoystickInstanceID(GlobalPdata.SDL_Data[joy].joystick);
	GlobalPdata.SDL_Data[joy].num_axes = controller ? 6 : SDL_JoystickNumAxes(GlobalPdata.SDL_Data[joy].joystick);
	GlobalPdata.SDL_Data[joy].num_buttons = controller ? 21 : SDL_JoystickNumButtons(GlobalPdata.SDL_Data[joy].joystick);
	GlobalPdata.SDL_Data[joy].num_hats = controller ? 1 : SDL_JoystickNumHats(GlobalPdata.SDL_Data[joy].joystick);
	GlobalPdata.SDL_Data[joy].num_balls = controller ? 0 : SDL_JoystickNumBalls(GlobalPdata.SDL_Data[joy].joystick);
	GlobalPdata.SDL_Data[joy].num_touchpads = controller ? SDL_GameControllerGetNumTouchpads(GlobalPdata.SDL_Data[joy].controller) : 0;
	GlobalPdata.SDL_Data[joy].haptic = SDL_HapticOpenFromJoystick(GlobalPdata.SDL_Data[joy].joystick);
	GlobalPdata.SDL_Data[joy].battery = SDL_JoystickCurrentPowerLevel(GlobalPdata.SDL_Data[joy].joystick);
	GlobalPdata.SDL_Data[joy].lastpressed = -1;
	GlobalPdata.SDL_Data[joy].lastreleased = -1;
	// Reset touchpads and fingers
	for (int i = 0; i < MAX_TOUCHPAD; i++)
	{
		GlobalPdata.SDL_Data[joy].touchpads[i].num_fingers = 0;
		for (int f = 0; f < MAX_FINGER; f++)
		{
			GlobalPdata.SDL_Data[joy].touchpads[i].finger[f].state = false;
			GlobalPdata.SDL_Data[joy].touchpads[i].finger[f].x = 0.0f;
			GlobalPdata.SDL_Data[joy].touchpads[i].finger[f].y = 0.0f;
			GlobalPdata.SDL_Data[joy].touchpads[i].finger[f].pressure = 0.0f;
		}
	}
	// Set touchpad fingers
	if (controller)
	{
		for (int i = 0; i < GlobalPdata.SDL_Data[joy].num_touchpads; i++)
			GlobalPdata.SDL_Data[joy].touchpads[i].num_fingers = SDL_GameControllerGetNumTouchpadFingers(GlobalPdata.SDL_Data[joy].controller, i);
	}
	// Reset axes
	for (int a = 0; a < MAX_AXIS; a++)
	{
		GlobalPdata.SDL_Data[joy].axis[a] = 0;
		bool has_initial = SDL_JoystickGetAxisInitialState(GlobalPdata.SDL_Data[joy].joystick, a, &GlobalPdata.SDL_Data[joy].axis_initial[a]);
		if (!has_initial)
			GlobalPdata.SDL_Data[joy].axis_initial[a] = 0;
	}
	// Reset buttons
	for (int h = 0; h < MAX_BUTTON; h++)
	{
		GlobalPdata.SDL_Data[joy].currentheld[h] = -1;
		GlobalPdata.SDL_Data[joy].held_buttons[h] = false;
		GlobalPdata.SDL_Data[joy].held_buttons_last[h] = false;
	}
	// Reset hats and balls
	for (int h = 0; h < MAX_HAT; h++)
	{
		GlobalPdata.SDL_Data[joy].hat[h] = 0;
	}
	for (int b = 0; b < MAX_BALL; b++)
	{
		GlobalPdata.SDL_Data[joy].ball_x[b] = 0;
		GlobalPdata.SDL_Data[joy].ball_y[b] = 0;
	}
	// Reset sensors
	GlobalPdata.SDL_Data[joy].accel.x = 0;
	GlobalPdata.SDL_Data[joy].accel.y = 0;
	GlobalPdata.SDL_Data[joy].accel.z = 0;
	GlobalPdata.SDL_Data[joy].accel_left.x = 0;
	GlobalPdata.SDL_Data[joy].accel_left.y = 0;
	GlobalPdata.SDL_Data[joy].accel_left.z = 0;
	GlobalPdata.SDL_Data[joy].accel_right.x = 0;
	GlobalPdata.SDL_Data[joy].accel_right.y = 0;
	GlobalPdata.SDL_Data[joy].accel_right.z = 0;
	GlobalPdata.SDL_Data[joy].gyro.x = 0;
	GlobalPdata.SDL_Data[joy].gyro.y = 0;
	GlobalPdata.SDL_Data[joy].gyro.z = 0;
	GlobalPdata.SDL_Data[joy].gyro_left.x = 0;
	GlobalPdata.SDL_Data[joy].gyro_left.y = 0;
	GlobalPdata.SDL_Data[joy].gyro_left.z = 0;
	GlobalPdata.SDL_Data[joy].gyro_right.x = 0;
	GlobalPdata.SDL_Data[joy].gyro_right.y = 0;
	GlobalPdata.SDL_Data[joy].gyro_right.z = 0;
	// Initialize haptic
	if (GlobalPdata.SDL_Data[joy].haptic != nullptr)
	{
		if (!SDL_HapticRumbleSupported(GlobalPdata.SDL_Data[joy].haptic))
		{
			SDL_HapticClose(GlobalPdata.SDL_Data[joy].haptic);
			GlobalPdata.SDL_Data[joy].haptic = nullptr;
		}
	}
	GlobalPdata.SDL_Data[joy].connected = true;
	GlobalPdata.SDL_Data[joy].which = -1;
	UpdateInput(joy);
}

// Update controller mapping for a device that is already plugged in
// Not sure how well this works...
void ReloadMapping(int joy)
{
	if (!GlobalPdata.SDL_Data[joy].connected || !GlobalPdata.SDLInitState)
		return;
	bool found = false;
	for (int i = 0; i < MAX_DEVICE && !found; i++)
	{
		if (!GlobalPdata.SDL_Data[joy].connected)
			continue;
		SDL_Joystick* joystick = SDL_JoystickOpen(i);
		SDL_JoystickID joystickID = SDL_JoystickInstanceID(joystick);
		SDL_JoystickClose(joystick);
		if (GlobalPdata.SDL_Data[joy].joy_id == joystickID)
		{
			found = true;
			OpenJoystick(joy, i, false);
		}
	}
}

// Adds an SDL_Event to the global data queue for Fusion to process.
// This is necessary because multiple SDL events can fire before Fusion gets to process them.
void AddEventToQueue(SDL_Event* event)
{
	for (int i = 0; i < MAX_EVENT; i++)
	{
		if (GlobalPdata.SDL_Events[i].type != SDL_FIRSTEVENT)
			continue;
		else
		{
			memcpy(&GlobalPdata.SDL_Events[i], event, sizeof(SDL_Event));
			return;
		}
	}
}

// Update values that aren't fetched directly from SDL.
void UpdateButtonStates(int joy)
{
	for (int b = 0; b < MAX_BUTTON; b++)
	{
		// Buttons released
		if (!GlobalPdata.SDL_Data[joy].held_buttons[b])
		{
			for (int hh2 = 0; hh2 < MAX_BUTTON; hh2++)
			{
				if (GlobalPdata.SDL_Data[joy].currentheld[hh2] == b)
				{
					GlobalPdata.SDL_Data[joy].currentheld[hh2] = -1;
					break;
				}
			}
		}
		// Buttons held
		if (GlobalPdata.SDL_Data[joy].held_buttons[b])
		{
			for (int hh2 = 0; hh2 < MAX_BUTTON; hh2++)
			{
				if (GlobalPdata.SDL_Data[joy].currentheld[hh2] == b)
					break;
				if (GlobalPdata.SDL_Data[joy].currentheld[hh2] == -1)
				{
					GlobalPdata.SDL_Data[joy].currentheld[hh2] = b;
					break;
				}
			}
		}
		// Buttons last pressed/released
		// If the button is currently held, update the last pressed state
		if (GlobalPdata.SDL_Data[joy].held_buttons[b])
		{
			if (!GlobalPdata.SDL_Data[joy].held_buttons_last[b])
				GlobalPdata.SDL_Data[joy].lastpressed = b;
		}
		// If the button is currently not held but was held in the previous frame, update the last released state
		else if (GlobalPdata.SDL_Data[joy].held_buttons_last[b])
			GlobalPdata.SDL_Data[joy].lastreleased = b;
	}
}

// Checks if a condition is true in the extension's device data (for normal conditions).
bool GetAxisData(long joy, long axis, long deadzone, bool positive)
{
	if (!GlobalPdata.SDL_Data[joy].connected)
		return false;
	Sint16 value = GlobalPdata.SDL_Data[joy].axis[axis];
	if (abs(value) < deadzone)
		return false;
	return positive ? (value > 0) : (value < 0);
}

// Goes through the extension's SDL event queue and returns true if a matching button event is found (for immediate conditions).
bool GetSDLEventButton(long joy, long button, bool pressed)
{
	if (!GlobalPdata.SDL_Data[joy].connected)
		return false;
	for (int i = 0; i < MAX_EVENT; i++)
	{
		// If there are no events left, the condition is false
		if (GlobalPdata.SDL_Events[i].common.type == SDL_FIRSTEVENT)
			return false;
		// Skip non-button events (pressed)
		if (pressed && GlobalPdata.SDL_Events[i].common.type != SDL_JOYBUTTONDOWN && GlobalPdata.SDL_Events[i].common.type != SDL_CONTROLLERBUTTONDOWN)
			continue;
		// Skip non-button events (released)
		if (!pressed && GlobalPdata.SDL_Events[i].common.type != SDL_JOYBUTTONUP && GlobalPdata.SDL_Events[i].common.type != SDL_CONTROLLERBUTTONUP)
			continue;
		// Skip events for wrong device ID
		if (GlobalPdata.SDL_Events[i].jbutton.which != GlobalPdata.SDL_Data[joy].joy_id)
			continue;
		// Skip events for wrong button ID
		if (GlobalPdata.SDL_Events[i].jbutton.button != button)
			continue;
		// Condition for pressed buttons
		if (pressed && GlobalPdata.SDL_Events[i].jbutton.state == SDL_PRESSED)
			return true;
		// Condition for released buttons
		if (!pressed && GlobalPdata.SDL_Events[i].jbutton.state == SDL_RELEASED)
			return true;
	}
	return false;
}

// Fires a Fusion event based on the button type.
void SendButtonEvent(LPRDATA rdPtr, long button)
{
	switch (button)
	{
	case SDL_CONTROLLER_BUTTON_DPAD_UP:
		callRunTimeFunction(rdPtr, RFUNCTION_GENERATEEVENT, CND_E_DPAD_UP, 0);
		break;
	case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
		callRunTimeFunction(rdPtr, RFUNCTION_GENERATEEVENT, CND_E_DPAD_DOWN, 0);
		break;
	case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
		callRunTimeFunction(rdPtr, RFUNCTION_GENERATEEVENT, CND_E_DPAD_LEFT, 0);
		break;
	case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
		callRunTimeFunction(rdPtr, RFUNCTION_GENERATEEVENT, CND_E_DPAD_RIGHT, 0);
		break;
	case SDL_CONTROLLER_BUTTON_A:
		callRunTimeFunction(rdPtr, RFUNCTION_GENERATEEVENT, CND_E_BUTTON_A, 0);
		break;
	case SDL_CONTROLLER_BUTTON_B:
		callRunTimeFunction(rdPtr, RFUNCTION_GENERATEEVENT, CND_E_BUTTON_B, 0);
		break;
	case SDL_CONTROLLER_BUTTON_X:
		callRunTimeFunction(rdPtr, RFUNCTION_GENERATEEVENT, CND_E_BUTTON_X, 0);
		break;
	case SDL_CONTROLLER_BUTTON_Y:
		callRunTimeFunction(rdPtr, RFUNCTION_GENERATEEVENT, CND_E_BUTTON_Y, 0);
		break;
	case SDL_CONTROLLER_BUTTON_START:
		callRunTimeFunction(rdPtr, RFUNCTION_GENERATEEVENT, CND_E_BUTTON_START, 0);
		break;
	case SDL_CONTROLLER_BUTTON_BACK:
		callRunTimeFunction(rdPtr, RFUNCTION_GENERATEEVENT, CND_E_BUTTON_BACK, 0);
		break;
	case SDL_CONTROLLER_BUTTON_GUIDE:
		callRunTimeFunction(rdPtr, RFUNCTION_GENERATEEVENT, CND_E_BUTTON_GUIDE, 0);
		break;
	case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
		callRunTimeFunction(rdPtr, RFUNCTION_GENERATEEVENT, CND_E_BUTTON_LEFTSHOULDER, 0);
		break;
	case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
		callRunTimeFunction(rdPtr, RFUNCTION_GENERATEEVENT, CND_E_BUTTON_RIGHTSHOULDER, 0);
		break;
	case SDL_CONTROLLER_BUTTON_LEFTSTICK:
		callRunTimeFunction(rdPtr, RFUNCTION_GENERATEEVENT, CND_E_BUTTON_LEFTSTICK, 0);
		break;
	case SDL_CONTROLLER_BUTTON_RIGHTSTICK:
		callRunTimeFunction(rdPtr, RFUNCTION_GENERATEEVENT, CND_E_BUTTON_RIGHTSTICK, 0);
		break;
	}
}

// Retrieves stored device sensor data.
// 'gyro': retrieve from gyroscope (true) or accelerometer (false).
// 'type': retrieve from main sensor (0), left sensor (1) or right sensor (2).
// 'xyz': retrieve X (0), Y (1) or Z (2) reading.
float GetSensorData(int joy, bool gyro, int type, int xyz)
{
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0.0f;
	switch (type)
	{
	case 0:
		if (xyz == 0)
			return gyro ? GlobalPdata.SDL_Data[joy].gyro.x : GlobalPdata.SDL_Data[joy].accel.x;
		else if (xyz == 1)
			return gyro ? GlobalPdata.SDL_Data[joy].gyro.y : GlobalPdata.SDL_Data[joy].accel.y;
		else if (xyz == 2)
			return gyro ? GlobalPdata.SDL_Data[joy].gyro.z : GlobalPdata.SDL_Data[joy].accel.z;
	case 1:
		if (xyz == 0)
			return gyro ? GlobalPdata.SDL_Data[joy].gyro_left.x : GlobalPdata.SDL_Data[joy].accel_left.x;
		else if (xyz == 1)
			return gyro ? GlobalPdata.SDL_Data[joy].gyro_left.y : GlobalPdata.SDL_Data[joy].accel_left.y;
		else if (xyz == 2)
			return gyro ? GlobalPdata.SDL_Data[joy].gyro_left.z : GlobalPdata.SDL_Data[joy].accel_left.z;
	case 2:
		if (xyz == 0)
			return gyro ? GlobalPdata.SDL_Data[joy].gyro_right.x : GlobalPdata.SDL_Data[joy].accel_right.x;
		else if (xyz == 1)
			return gyro ? GlobalPdata.SDL_Data[joy].gyro_right.y : GlobalPdata.SDL_Data[joy].accel_right.y;
		else if (xyz == 2)
			return gyro ? GlobalPdata.SDL_Data[joy].gyro_right.z : GlobalPdata.SDL_Data[joy].accel_right.z;
	}
	return 0.0f;
}

// Finds a device by Joystick instance ID.
// This is necessary to match the 'which' in SDL events with the array index of SDLData.
int FindJoystick(int which)
{
	for (int j = 0; j < MAX_DEVICE; j++)
	{
		if (!GlobalPdata.SDL_Data[j].connected)
			continue;
		if (GlobalPdata.SDL_Data[j].joy_id == which)
			return j;
	}
	return -1;
}