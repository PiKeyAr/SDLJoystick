// ============================================================================
//
// This file are where the Conditions/Actions/Expressions are defined.
// You can manually enter these, or use CICK (recommended)
// See the Extension FAQ in this SDK for more info and where to download it
//
// ============================================================================

// Common Include
#include "common.h"
#include "SDL.h"
#include "GlobalData.h"
char* WcharToChar(const wchar_t* src, size_t src_length, size_t* out_length);
void ReloadMapping(int joy);
bool GetSDLEventButton(long joy, long button, bool pressed);
bool GetAxisData(long joy, long axis, long deadzone, bool positive);
float GetSensorData(int joy, bool gyro, int type, int xyz);
void UpdateInput(int i);

// Quick memo: content of the eventInformations arrays
// ---------------------------------------------------
// Menu ID
// String ID
// Code
// Flags
// Number_of_parameters
// Parameter_type [Number_of_parameters]
// Parameter_TitleString [Number_of_parameters]

// Definitions of parameters for each condition
short conditionsInfos[]=
		{
		// Before 2.00
		IDMN_ISCONNECTED, M_ISCONNECTED, CND_ISCONNECTED, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_E_BUTTONDOWN, M_E_BUTTONDOWN, CND_E_BUTTONDOWN, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_BUTTON,
		IDMN_BUTTON_HOLD, M_BUTTON_HOLD, CND_BUTTON_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_BUTTON,
		IDMN_E_BUTTONUP, M_E_BUTTONUP, CND_E_BUTTONUP, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_BUTTON,
		IDMN_E_BUTTONDOWN_ANY, M_E_BUTTONDOWN_ANY, CND_E_BUTTONDOWN_ANY, 0, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_BUTTON_ANY_HOLD, M_BUTTON_ANY_HOLD, CND_BUTTON_ANY_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_E_BUTTONUP_ANY, M_E_BUTTONUP_ANY, CND_E_BUTTONUP_ANY, 0, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_ISCONTROLLER, M_ISCONTROLLER, CND_ISCONTROLLER, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_HASRUMBLE, M_HASRUMBLE, CND_HASRUMBLE, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		// After 2.00 - devices
		IDMN_E_DEVICEADDED, M_E_DEVICEADDED, CND_E_DEVICEADDED, 0, 0,
		IDMN_E_DEVICEREMOVED, M_E_DEVICEREMOVED, CND_E_DEVICEREMOVED, 0, 0,
		// After 2.00 - axes
		IDMN_E_AXISMOTION, M_E_AXISMOTION, CND_E_AXISMOTION, 0, 3, PARAM_EXPRESSION, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_AXIS, M_CND_DEADZONE,
		IDMN_LEFTSTICK_UP_HOLD, M_LEFTSTICK_UP_HOLD, CND_LEFTSTICK_UP_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_DEADZONE,
		IDMN_LEFTSTICK_DOWN_HOLD, M_LEFTSTICK_DOWN_HOLD, CND_LEFTSTICK_DOWN_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_DEADZONE,
		IDMN_LEFTSTICK_LEFT_HOLD, M_LEFTSTICK_LEFT_HOLD, CND_LEFTSTICK_LEFT_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_DEADZONE,
		IDMN_LEFTSTICK_RIGHT_HOLD, M_LEFTSTICK_RIGHT_HOLD, CND_LEFTSTICK_RIGHT_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_DEADZONE,
		IDMN_RIGHTSTICK_UP_HOLD, M_RIGHTSTICK_UP_HOLD, CND_RIGHTSTICK_UP_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_DEADZONE,
		IDMN_RIGHTSTICK_DOWN_HOLD, M_RIGHTSTICK_DOWN_HOLD, CND_RIGHTSTICK_DOWN_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_DEADZONE,
		IDMN_RIGHTSTICK_LEFT_HOLD, M_RIGHTSTICK_LEFT_HOLD, CND_RIGHTSTICK_LEFT_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_DEADZONE,
		IDMN_RIGHTSTICK_RIGHT_HOLD, M_RIGHTSTICK_RIGHT_HOLD, CND_RIGHTSTICK_RIGHT_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_DEADZONE,
		IDMN_TRIGGER_LEFT_HOLD, M_TRIGGER_LEFT_HOLD, CND_TRIGGER_LEFT_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 3, PARAM_EXPRESSION, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_DEADZONE, M_CND_HELDSTATE,
		IDMN_TRIGGER_RIGHT_HOLD, M_TRIGGER_RIGHT_HOLD, CND_TRIGGER_RIGHT_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 3, PARAM_EXPRESSION, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_DEADZONE, M_CND_HELDSTATE,
		IDMN_AXIS_HOLD, M_AXIS_HOLD, CND_AXIS_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 4, PARAM_EXPRESSION, PARAM_EXPRESSION, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_AXIS, M_CND_DEADZONE, M_CND_AXISDIRECTION,
		// After 2.00 - hat
		IDMN_E_JOYHATMOTION, M_E_JOYHATMOTION, CND_E_JOYHATMOTION, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_HAT,
		// After 2.00 - buttons
		IDMN_E_BUTTON_A, M_E_BUTTON_A, CND_E_BUTTON_A, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_HELDSTATE,
		IDMN_E_BUTTON_B, M_E_BUTTON_B, CND_E_BUTTON_B, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_HELDSTATE,
		IDMN_E_BUTTON_X, M_E_BUTTON_X, CND_E_BUTTON_X, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_HELDSTATE,
		IDMN_E_BUTTON_Y, M_E_BUTTON_Y, CND_E_BUTTON_Y, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_HELDSTATE,
		IDMN_E_BUTTON_START, M_E_BUTTON_START, CND_E_BUTTON_START, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_HELDSTATE,
		IDMN_E_BUTTON_BACK, M_E_BUTTON_BACK, CND_E_BUTTON_BACK, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_HELDSTATE,
		IDMN_E_BUTTON_GUIDE, M_E_BUTTON_GUIDE, CND_E_BUTTON_GUIDE, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_HELDSTATE,
		IDMN_E_BUTTON_LEFTSTICK, M_E_BUTTON_LEFTSTICK, CND_E_BUTTON_LEFTSTICK, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_HELDSTATE,
		IDMN_E_BUTTON_RIGHTSTICK, M_E_BUTTON_RIGHTSTICK, CND_E_BUTTON_RIGHTSTICK, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_HELDSTATE,
		IDMN_E_BUTTON_LEFTSHOULDER, M_E_BUTTON_LEFTSHOULDER, CND_E_BUTTON_LEFTSHOULDER, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_HELDSTATE,
		IDMN_E_BUTTON_RIGHTSHOULDER, M_E_BUTTON_RIGHTSHOULDER, CND_E_BUTTON_RIGHTSHOULDER, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_HELDSTATE,
		IDMN_E_DPAD_UP, M_E_DPAD_UP, CND_E_DPAD_UP, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_HELDSTATE,
		IDMN_E_DPAD_DOWN, M_E_DPAD_DOWN, CND_E_DPAD_DOWN, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_HELDSTATE,
		IDMN_E_DPAD_LEFT, M_E_DPAD_LEFT, CND_E_DPAD_LEFT, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_HELDSTATE,
		IDMN_E_DPAD_RIGHT, M_E_DPAD_RIGHT, CND_E_DPAD_RIGHT, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_HELDSTATE,
		// After 2.00 - held buttons
		IDMN_BUTTON_A_HOLD, M_BUTTON_A_HOLD, CND_BUTTON_A_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_BUTTON_B_HOLD, M_BUTTON_B_HOLD, CND_BUTTON_B_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_BUTTON_X_HOLD, M_BUTTON_X_HOLD, CND_BUTTON_X_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_BUTTON_Y_HOLD, M_BUTTON_Y_HOLD, CND_BUTTON_Y_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_BUTTON_START_HOLD, M_BUTTON_START_HOLD, CND_BUTTON_START_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_BUTTON_BACK_HOLD, M_BUTTON_BACK_HOLD, CND_BUTTON_BACK_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_BUTTON_GUIDE_HOLD, M_BUTTON_GUIDE_HOLD, CND_BUTTON_GUIDE_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_BUTTON_LEFTSTICK_HOLD, M_BUTTON_LEFTSTICK_HOLD, CND_BUTTON_LEFTSTICK_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_BUTTON_RIGHTSTICK_HOLD, M_BUTTON_RIGHTSTICK_HOLD, CND_BUTTON_RIGHTSTICK_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_BUTTON_LEFTSHOULDER_HOLD, M_BUTTON_LEFTSHOULDER_HOLD, CND_BUTTON_LEFTSHOULDER_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_BUTTON_RIGHTSHOULDER_HOLD, M_BUTTON_RIGHTSHOULDER_HOLD, CND_BUTTON_RIGHTSHOULDER_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_DPAD_UP_HOLD, M_DPAD_UP_HOLD, CND_DPAD_UP_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_DPAD_DOWN_HOLD, M_DPAD_DOWN_HOLD, CND_DPAD_DOWN_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_DPAD_LEFT_HOLD, M_DPAD_LEFT_HOLD, CND_DPAD_LEFT_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_DPAD_RIGHT_HOLD, M_DPAD_RIGHT_HOLD, CND_DPAD_RIGHT_HOLD, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		// After 2.10
		IDMN_HASLED, M_HASLED, CND_HASLED, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_E_BALLMOTION, M_E_BALLMOTION, CND_E_BALLMOTION, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_BALL,
		IDMN_E_TOUCHPAD, M_E_TOUCHPAD, CND_E_TOUCHPAD, 0, 3, PARAM_EXPRESSION, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_TOUCHPAD, M_CND_HELDSTATE,
		IDMN_E_FINGER, M_E_FINGER, CND_E_FINGER, 0, 4, PARAM_EXPRESSION, PARAM_EXPRESSION, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_TOUCHPAD, M_CND_FINGER, M_CND_HELDSTATE,
		IDMN_E_TOUCHPADMOTION, M_E_TOUCHPADMOTION, CND_E_TOUCHPADMOTION, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_TOUCHPAD,
		IDMN_E_FINGERMOTION, M_E_FINGERMOTION, CND_E_FINGERMOTION, 0, 3, PARAM_EXPRESSION, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_TOUCHPAD, M_CND_FINGER,
		IDMN_E_BATTERY, M_E_BATTERY, CND_E_BATTERY, 0, 1, PARAM_EXPRESSION, M_CND_JOY,
		// After 2.20
		IDMN_HASTRIGGERRUMBLE, M_HASTRIGGERRUMBLE, CND_HASTRIGGERRUMBLE, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_HASACCEL, M_HASACCEL, CND_HASACCEL, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_ACCEL,
		IDMN_HASACCELENABLED, M_HASACCELENABLED, CND_HASACCELENABLED, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_ACCEL,
		IDMN_HASGYRO, M_HASGYRO, CND_HASGYRO, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_GYRO,
		IDMN_HASGYROENABLED, M_HASGYROENABLED, CND_HASGYROENABLED, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_CND_JOY, M_CND_GYRO,
		IDMN_E_REMAPPED, M_E_REMAPPED, CND_E_REMAPPED, 0, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_E_REMAPPED_ANY, M_E_REMAPPED_ANY, CND_E_REMAPPED_ANY, 0, 0,
		IDMN_E_SENSOR, M_E_SENSOR, CND_E_SENSOR, 0, 1, PARAM_EXPRESSION, M_CND_JOY,
		IDMN_ISGAMECONTROLLERENABLED, M_ISGAMECONTROLLERENABLED, CND_ISGAMECONTROLLERENABLED, EVFLAGS_ALWAYS + EVFLAGS_NOTABLE, 1, PARAM_EXPRESSION, M_CND_JOY,
		};

// Definitions of parameters for each action
short actionsInfos[]=
		{
		IDMN_LOADDB, M_LOADDB, ACT_LOADDB, 0, 1, PARAM_FILENAME2, M_ACT_LOADDB,
		IDMN_RUMBLE, M_RUMBLE, ACT_RUMBLE, 0, 4, PARAM_EXPRESSION, PARAM_EXPRESSION, PARAM_EXPRESSION, PARAM_EXPRESSION, M_ACT_JOY, M_ACT_RUMBLE_WEAK, M_ACT_RUMBLE_STRONG, M_ACT_RUMBLE_DURATION,
		IDMN_SETLED, M_SETLED, ACT_SETLED, 0, 2, PARAM_EXPRESSION, PARAM_COLOUR, M_ACT_JOY, M_ACT_COLOR,
		IDMN_SETMAPPING, M_SETMAPPING, ACT_SETMAPPING, 0, 3, PARAM_EXPSTRING, PARAM_EXPSTRING, PARAM_EXPSTRING, M_ACT_GUID, M_ACT_MAPPINGNAME, M_ACT_MAPPING_DEVICE,
		IDMN_SETMAPPING_DEVICE, M_SETMAPPING_DEVICE, ACT_SETMAPPING_DEVICE, 0, 2, PARAM_EXPRESSION, PARAM_EXPSTRING, M_ACT_JOY, M_ACT_MAPPING_DEVICE,
		IDMN_TRIGGERRUMBLE, M_TRIGGERRUMBLE, ACT_TRIGGERRUMBLE, 0, 4, PARAM_EXPRESSION, PARAM_EXPRESSION, PARAM_EXPRESSION, PARAM_EXPRESSION, M_ACT_JOY, M_ACT_RUMBLE_WEAK, M_ACT_RUMBLE_STRONG, M_ACT_RUMBLE_DURATION,
		IDMN_SETPLAYER, M_SETPLAYER, ACT_SETPLAYER, 0, 2, PARAM_EXPRESSION, PARAM_EXPRESSION, M_ACT_JOY, M_ACT_PLAYER,
		IDMN_SETACCEL, M_SETACCEL, ACT_SETACCEL, 0, 3, PARAM_EXPRESSION, PARAM_EXPRESSION, PARAM_EXPRESSION, M_ACT_JOY, M_ACT_ACCEL, M_ACT_STATE,
		IDMN_SETGYRO, M_SETGYRO, ACT_SETGYRO, 0, 3, PARAM_EXPRESSION, PARAM_EXPRESSION, PARAM_EXPRESSION, M_ACT_JOY, M_ACT_GYRO, M_ACT_STATE,
		IDMN_POLL, M_POLL, ACT_POLL, 0, 0,
		};

// Definitions of parameters for each expression
short expressionsInfos[]=
		{
		IDMN_NUMJOYS, M_NUMJOYS, EXP_NUMJOYS, 0, 0,
		IDMN_GETAXIS, M_GETAXIS, EXP_GETAXIS, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETBUTTON, M_GETBUTTON, EXP_GETBUTTON, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETHAT, M_GETHAT, EXP_GETHAT, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETBALLX, M_GETBALLX, EXP_GETBALLX, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETBALLY, M_GETBALLY, EXP_GETBALLY, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_NUMAXES, M_NUMAXES, EXP_NUMAXES, 0, 1, EXPPARAM_LONG, 0,
		IDMN_NUMBUTTONS, M_NUMBUTTONS, EXP_NUMBUTTONS, 0, 1, EXPPARAM_LONG, 0,
		IDMN_NUMHATS, M_NUMHATS, EXP_NUMHATS, 0, 1, EXPPARAM_LONG, 0,
		IDMN_NUMBALLS, M_NUMBALLS, EXP_NUMBALLS, 0, 1, EXPPARAM_LONG, 0,
		IDMN_DEVICENAME, M_DEVICENAME, EXP_DEVICENAME, EXPFLAG_STRING, 1, EXPPARAM_LONG, 0,
		IDMN_DEVICEGUID, M_DEVICEGUID, EXP_DEVICEGUID, EXPFLAG_STRING, 1, EXPPARAM_LONG, 0,
		IDMN_GETBUTTONSHELD, M_GETBUTTONSHELD, EXP_GETBUTTONSHELD, EXPFLAG_STRING, 1, EXPPARAM_LONG, 0,
		IDMN_HELDBUTTON, M_HELDBUTTON, EXP_HELDBUTTON, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_LASTPRESSED, M_LASTPRESSED, EXP_LASTPRESSED, 0, 1, EXPPARAM_LONG, 0,
		IDMN_LASTRELEASED, M_LASTRELEASED, EXP_LASTRELEASED, 0, 1, EXPPARAM_LONG, 0,
		IDMN_GETAXISINITIAL, M_GETAXISINITIAL, EXP_GETAXISINITIAL, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETBATTERYLEVEL, M_GETBATTERYLEVEL, EXP_GETBATTERYLEVEL, 0, 1, EXPPARAM_LONG, 0,
		IDMN_NUMTOUCHPADS, M_NUMTOUCHPADS, EXP_NUMTOUCHPADS, 0, 1, EXPPARAM_LONG, 0,
		IDMN_NUMFINGERS, M_NUMFINGERS, EXP_NUMFINGERS, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETFINGERSTATE, M_GETFINGERSTATE, EXP_GETFINGERSTATE, 0, 3, EXPPARAM_LONG, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0, 0,
		IDMN_GETFINGERX, M_GETFINGERX, EXP_GETFINGERX, 0, 3, EXPPARAM_LONG, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0, 0,
		IDMN_GETFINGERY, M_GETFINGERY, EXP_GETFINGERY, 0, 3, EXPPARAM_LONG, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0, 0,
		IDMN_GETFINGERPRESSURE, M_GETFINGERPRESSURE, EXP_GETFINGERPRESSURE, 0, 3, EXPPARAM_LONG, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0, 0,
		IDMN_JOYTYPE, M_JOYTYPE, EXP_JOYTYPE, 0, 1, EXPPARAM_LONG, 0,
		IDMN_CONTROLLERTYPE, M_CONTROLLERTYPE, EXP_CONTROLLERTYPE, 0, 1, EXPPARAM_LONG, 0,
		IDMN_GETPLAYER, M_GETPLAYER, EXP_GETPLAYER, 0, 1, EXPPARAM_LONG, 0,
		IDMN_VENDOR, M_VENDOR, EXP_VENDOR, 0, 1, EXPPARAM_LONG, 0,
		IDMN_PRODUCT, M_PRODUCT, EXP_PRODUCT, 0, 1, EXPPARAM_LONG, 0,
		IDMN_PRODUCTVERSION, M_PRODUCTVERSION, EXP_PRODUCTVERSION, 0, 1, EXPPARAM_LONG, 0,
		IDMN_FIRMWARE, M_FIRMWARE, EXP_FIRMWARE, 0, 1, EXPPARAM_LONG, 0,
		IDMN_SERIAL, M_SERIAL, EXP_SERIAL, EXPFLAG_STRING, 1, EXPPARAM_LONG, 0,
		IDMN_GETACCELSTATE, M_GETACCELSTATE, EXP_GETACCELSTATE, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETACCELX, M_GETACCELX, EXP_GETACCELX, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETACCELY, M_GETACCELY, EXP_GETACCELY, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETACCELZ, M_GETACCELZ, EXP_GETACCELZ, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETGYROSTATE, M_GETGYROSTATE, EXP_GETGYROSTATE, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETGYROX, M_GETGYROX, EXP_GETGYROX, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETGYROY, M_GETGYROY, EXP_GETGYROY, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETGYROZ, M_GETGYROZ, EXP_GETGYROZ, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETBINDSTRING_DEVICE, M_GETBINDSTRING_DEVICE, EXP_GETBINDSTRING_DEVICE, EXPFLAG_STRING, 1, EXPPARAM_LONG, 0,
		IDMN_GETBINDSTRING_AXIS, M_GETBINDSTRING_AXIS, EXP_GETBINDSTRING_AXIS, EXPFLAG_STRING, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETBINDSTRING_BUTTON, M_GETBINDSTRING_BUTTON, EXP_GETBINDSTRING_BUTTON, EXPFLAG_STRING, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETBINDTYPE_AXIS, M_GETBINDTYPE_AXIS, EXP_GETBINDTYPE_AXIS, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETBINDVALUE_AXIS, M_GETBINDVALUE_AXIS, EXP_GETBINDVALUE_AXIS, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETBINDTYPE_BUTTON, M_GETBINDTYPE_BUTTON, EXP_GETBINDTYPE_BUTTON, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		IDMN_GETBINDVALUE_BUTTON, M_GETBINDVALUE_BUTTON, EXP_GETBINDVALUE_BUTTON, 0, 2, EXPPARAM_LONG, EXPPARAM_LONG, 0, 0,
		};

// ============================================================================
//
// CONDITION ROUTINES
// 
// ============================================================================

long WINAPI DLLExport IsConnected(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	return GlobalPdata.SDL_Data[joy].connected;
}

long WINAPI DLLExport IsGameController(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	if (!GlobalPdata.SDLInitState)
		return 0;
	return (long)GlobalPdata.SDL_Data[joy].is_controller;
}

long WINAPI DLLExport IsGameControllerEnabled(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	return (long)(GlobalPdata.SDL_Data[joy].controller != nullptr);
}

long WINAPI DLLExport HasRumble(LPRDATA rdPtr, long param1, long param2)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	if (GlobalPdata.SDL_Data[joy].controller != nullptr)
		return SDL_GameControllerHasRumble(GlobalPdata.SDL_Data[joy].controller);
	else if (GlobalPdata.SDL_Data[joy].joystick != nullptr)
		return SDL_JoystickHasRumble(GlobalPdata.SDL_Data[joy].joystick);
	return 0;
}

long WINAPI DLLExport ButtonHeldDown(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long button = max(0, min(MAX_BUTTON - 1, param2));
	if (GlobalPdata.SDL_Data[joy].held_buttons[button])
		return 1;
	else
		return 0;
}

long WINAPI DLLExport AnyButtonHeldDown(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	for (int i = 0; i < MAX_BUTTON; i++)
	{
		if (GlobalPdata.SDL_Data[joy].held_buttons[i])
			return 1;
	}
	return 0;
}

long WINAPI DLLExport SDLEventAxisMotion(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long axis = CNC_GetIntParameter(rdPtr);
	long deadzone = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	axis = max(0, min(MAX_AXIS - 1 - 1, axis));
	deadzone = max(0, min(32767, deadzone));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	for (int i = 0; i < MAX_EVENT; i++)
	{
		// If there are no events left, the condition is false
		if (GlobalPdata.SDL_Events[i].common.type == SDL_FIRSTEVENT)
			return 0;
		// Event ID, joystick instance ID and axis ID must match + deadzone check
		if (GlobalPdata.SDL_Events[i].common.type == SDL_JOYAXISMOTION || GlobalPdata.SDL_Events[i].common.type == SDL_CONTROLLERAXISMOTION)
		{
			if (GlobalPdata.SDL_Events[i].jaxis.which == GlobalPdata.SDL_Data[joy].joy_id &&
				GlobalPdata.SDL_Events[i].jaxis.axis == axis &&
				abs(GlobalPdata.SDL_Events[i].jaxis.value) > deadzone)
				return 1;
		}
	}
	return 0;
}

long WINAPI DLLExport SDLEventJoystickHatMotion(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long hat = max(0, min(MAX_HAT - 1, param2));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	if (GlobalPdata.SDL_Data[joy].controller != nullptr) // This condition should not trigger for GameControllers
		return 0;
	for (int i = 0; i < MAX_EVENT; i++)
	{
		// If there are no events left, the condition is false
		if (GlobalPdata.SDL_Events[i].common.type == SDL_FIRSTEVENT)
			return 0;
		// Event ID, joystick instance ID and hat ID must match
		if (GlobalPdata.SDL_Events[i].common.type == SDL_JOYHATMOTION &&
			GlobalPdata.SDL_Events[i].jhat.which == GlobalPdata.SDL_Data[joy].joy_id &&
			GlobalPdata.SDL_Events[i].jhat.hat == hat)
			return 1;
	}
	return 0;
}

long WINAPI DLLExport SDLEventButtonDown(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long button = max(0, min(MAX_BUTTON - 1, param2));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	for (int i = 0; i < MAX_EVENT; i++)
	{
		// If there are no events left, the condition is false
		if (GlobalPdata.SDL_Events[i].common.type == SDL_FIRSTEVENT)
			return 0;
		// Event ID, joystick instance ID and button ID must match
		if (GlobalPdata.SDL_Events[i].common.type == SDL_JOYBUTTONDOWN || GlobalPdata.SDL_Events[i].common.type == SDL_CONTROLLERBUTTONDOWN)
		{
			if (GlobalPdata.SDL_Events[i].jbutton.which == GlobalPdata.SDL_Data[joy].joy_id &&
				GlobalPdata.SDL_Events[i].jbutton.button == button)
				return (long)(GlobalPdata.SDL_Events[i].jbutton.state == SDL_PRESSED);
		}
	}
	return 0;
}

long WINAPI DLLExport SDLEventAnyButtonDown(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	for (int i = 0; i < MAX_EVENT; i++)
	{
		// If there are no events left, the condition is false
		if (GlobalPdata.SDL_Events[i].common.type == SDL_FIRSTEVENT)
			return 0;
		// Event ID, joystick instance ID and button ID must match
		if (GlobalPdata.SDL_Events[i].common.type == SDL_JOYBUTTONDOWN || GlobalPdata.SDL_Events[i].common.type == SDL_CONTROLLERBUTTONDOWN)
		{
			if (GlobalPdata.SDL_Events[i].jbutton.which == GlobalPdata.SDL_Data[joy].joy_id && 
				GlobalPdata.SDL_Events[i].jbutton.state == SDL_PRESSED)
				return 1;
		}
	}
	return 0;
}

long WINAPI DLLExport SDLEventButtonUp(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long button = max(0, min(MAX_BUTTON - 1, param2));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	for (int i = 0; i < MAX_EVENT; i++)
	{
		// If there are no events left, the condition is false
		if (GlobalPdata.SDL_Events[i].common.type == SDL_FIRSTEVENT)
			return 0;
		// Event ID, joystick instance ID and button ID must match
		if (GlobalPdata.SDL_Events[i].common.type == SDL_JOYBUTTONUP || GlobalPdata.SDL_Events[i].common.type == SDL_CONTROLLERBUTTONUP)
		{
			if (GlobalPdata.SDL_Events[i].jbutton.which == GlobalPdata.SDL_Data[joy].joy_id &&
				GlobalPdata.SDL_Events[i].jbutton.button == button)
				return (long)(GlobalPdata.SDL_Events[i].jbutton.state == SDL_RELEASED);
		}
	}
	return 0;
}

long WINAPI DLLExport SDLEventAnyButtonUp(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long button = max(0, min(MAX_BUTTON - 1, param2));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	for (int i = 0; i < MAX_EVENT; i++)
	{
		// If there are no events left, the condition is false
		if (GlobalPdata.SDL_Events[i].common.type == SDL_FIRSTEVENT)
			return 0;
		// Event ID, joystick instance ID and button ID must match
		if (GlobalPdata.SDL_Events[i].common.type == SDL_JOYBUTTONUP || GlobalPdata.SDL_Events[i].common.type == SDL_CONTROLLERBUTTONUP)
		{
			if (GlobalPdata.SDL_Events[i].jbutton.which == GlobalPdata.SDL_Data[joy].joy_id &&
				GlobalPdata.SDL_Events[i].jbutton.state == SDL_RELEASED)
				return 1;
		}
	}
	return 0;
}

long WINAPI DLLExport SDLEventDeviceAdded(LPRDATA rdPtr, long param1, long param2)
{	
	for (int i = 0; i < MAX_EVENT; i++)
	{
		// If there are no events left, the condition is false
		if (GlobalPdata.SDL_Events[i].common.type == SDL_FIRSTEVENT)
			return 0;
		if (GlobalPdata.SDL_Events[i].common.type == SDL_JOYDEVICEADDED)
			return 1;
	}
	return 0;
}

long WINAPI DLLExport SDLEventDeviceRemoved(LPRDATA rdPtr, long param1, long param2)
{
	for (int i = 0; i < MAX_EVENT; i++)
	{
		// If there are no events left, the condition is false
		if (GlobalPdata.SDL_Events[i].common.type == SDL_FIRSTEVENT)
			return 0;
		if (GlobalPdata.SDL_Events[i].common.type == SDL_JOYDEVICEREMOVED)
			return 1;
	}
	return 0;
}

long WINAPI DLLExport LeftStickUp(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long deadzone = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	deadzone = max(0, min(32767, deadzone));
	return (long)GetAxisData(joy, SDL_CONTROLLER_AXIS_LEFTY, deadzone, false);
}

long WINAPI DLLExport LeftStickDown(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long deadzone = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	deadzone = max(0, min(32767, deadzone));
	return (long)GetAxisData(joy, SDL_CONTROLLER_AXIS_LEFTY, deadzone, true);
}

long WINAPI DLLExport LeftStickLeft(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long deadzone = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	deadzone = max(0, min(32767, deadzone));
	return (long)GetAxisData(joy, SDL_CONTROLLER_AXIS_LEFTX, deadzone, false);
}

long WINAPI DLLExport LeftStickRight(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long deadzone = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	deadzone = max(0, min(32767, deadzone));
	return (long)GetAxisData(joy, SDL_CONTROLLER_AXIS_LEFTX, deadzone, true);
}

long WINAPI DLLExport RightStickUp(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long deadzone = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	deadzone = max(0, min(32767, deadzone));
	return (long)GetAxisData(joy, SDL_CONTROLLER_AXIS_RIGHTY, deadzone, false);
}

long WINAPI DLLExport RightStickDown(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long deadzone = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	deadzone = max(0, min(32767, deadzone));
	return (long)GetAxisData(joy, SDL_CONTROLLER_AXIS_RIGHTY, deadzone, true);
}

long WINAPI DLLExport RightStickLeft(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long deadzone = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	deadzone = max(0, min(32767, deadzone));
	return (long)GetAxisData(joy, SDL_CONTROLLER_AXIS_RIGHTX, deadzone, false);
}

long WINAPI DLLExport RightStickRight(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long deadzone = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	deadzone = max(0, min(32767, deadzone));
	return (long)GetAxisData(joy, SDL_CONTROLLER_AXIS_RIGHTX, deadzone, true);
}

long WINAPI DLLExport SDLEventDpadUp(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	return (long)GetSDLEventButton(joy, SDL_CONTROLLER_BUTTON_DPAD_UP, state);
}

long WINAPI DLLExport SDLEventDpadDown(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	return (long)GetSDLEventButton(joy, SDL_CONTROLLER_BUTTON_DPAD_DOWN, state);
}

long WINAPI DLLExport SDLEventDpadLeft(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	return (long)GetSDLEventButton(joy, SDL_CONTROLLER_BUTTON_DPAD_LEFT, state);
}

long WINAPI DLLExport SDLEventDpadRight(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	return (long)GetSDLEventButton(joy, SDL_CONTROLLER_BUTTON_DPAD_RIGHT, state);
}

long WINAPI DLLExport SDLEventButtonA(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	return (long)GetSDLEventButton(joy, SDL_CONTROLLER_BUTTON_A, state);
}

long WINAPI DLLExport SDLEventButtonB(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	return (long)GetSDLEventButton(joy, SDL_CONTROLLER_BUTTON_B, state);
}

long WINAPI DLLExport SDLEventButtonX(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	return (long)GetSDLEventButton(joy, SDL_CONTROLLER_BUTTON_X, state);
}

long WINAPI DLLExport SDLEventButtonY(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	return (long)GetSDLEventButton(joy, SDL_CONTROLLER_BUTTON_Y, state);
}

long WINAPI DLLExport SDLEventButtonStart(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	return (long)GetSDLEventButton(joy, SDL_CONTROLLER_BUTTON_START, state);
}

long WINAPI DLLExport SDLEventButtonBack(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	return (long)GetSDLEventButton(joy, SDL_CONTROLLER_BUTTON_BACK, state);
}

long WINAPI DLLExport SDLEventButtonGuide(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	return (long)GetSDLEventButton(joy, SDL_CONTROLLER_BUTTON_GUIDE, state);
}

long WINAPI DLLExport SDLEventButtonLeftShoulder(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	return (long)GetSDLEventButton(joy, SDL_CONTROLLER_BUTTON_LEFTSHOULDER, state);
}

long WINAPI DLLExport SDLEventButtonRightShoulder(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	return (long)GetSDLEventButton(joy, SDL_CONTROLLER_BUTTON_RIGHTSHOULDER, state);
}

long WINAPI DLLExport SDLEventButtonLeftStick(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	return (long)GetSDLEventButton(joy, SDL_CONTROLLER_BUTTON_LEFTSTICK, state);
}

long WINAPI DLLExport SDLEventButtonRightStick(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	return (long)GetSDLEventButton(joy, SDL_CONTROLLER_BUTTON_RIGHTSTICK, state);
}

long WINAPI DLLExport TriggerLeft(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long deadzone = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	deadzone = max(0, min(32767, deadzone));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	return (long)GetAxisData(joy, SDL_CONTROLLER_AXIS_TRIGGERLEFT, deadzone, state);
}

long WINAPI DLLExport TriggerRight(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long deadzone = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	deadzone = max(0, min(32767, deadzone));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	return (long)GetAxisData(joy, SDL_CONTROLLER_AXIS_TRIGGERRIGHT, deadzone, state);
}

long WINAPI DLLExport AxisHold(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long axis = CNC_GetIntParameter(rdPtr);
	long deadzone = CNC_GetIntParameter(rdPtr);
	long direction = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	axis = max(0, min(MAX_AXIS - 1, axis));
	deadzone = max(0, min(32767, deadzone));
	direction = max(SDL_RELEASED, min(SDL_PRESSED, direction));
	return (long)GetAxisData(joy, axis, deadzone, direction);
}

long WINAPI DLLExport HoldButtonA(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long button = SDL_CONTROLLER_BUTTON_A;
	if (GlobalPdata.SDL_Data[joy].held_buttons[button])
		return 1;
	else
		return 0;
}

long WINAPI DLLExport HoldButtonB(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long button = SDL_CONTROLLER_BUTTON_B;
	if (GlobalPdata.SDL_Data[joy].held_buttons[button])
		return 1;
	else
		return 0;
}

long WINAPI DLLExport HoldButtonX(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long button = SDL_CONTROLLER_BUTTON_X;
	if (GlobalPdata.SDL_Data[joy].held_buttons[button])
		return 1;
	else
		return 0;
}

long WINAPI DLLExport HoldButtonY(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long button = SDL_CONTROLLER_BUTTON_Y;
	if (GlobalPdata.SDL_Data[joy].held_buttons[button])
		return 1;
	else
		return 0;
}

long WINAPI DLLExport HoldButtonStart(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long button = SDL_CONTROLLER_BUTTON_START;
	if (GlobalPdata.SDL_Data[joy].held_buttons[button])
		return 1;
	else
		return 0;
}

long WINAPI DLLExport HoldButtonBack(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long button = SDL_CONTROLLER_BUTTON_BACK;
	if (GlobalPdata.SDL_Data[joy].held_buttons[button])
		return 1;
	else
		return 0;
}

long WINAPI DLLExport HoldButtonGuide(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long button = SDL_CONTROLLER_BUTTON_GUIDE;
	if (GlobalPdata.SDL_Data[joy].held_buttons[button])
		return 1;
	else
		return 0;
}

long WINAPI DLLExport HoldButtonLeftStick(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long button = SDL_CONTROLLER_BUTTON_LEFTSTICK;
	if (GlobalPdata.SDL_Data[joy].held_buttons[button])
		return 1;
	else
		return 0;
}

long WINAPI DLLExport HoldButtonRightStick(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long button = SDL_CONTROLLER_BUTTON_RIGHTSTICK;
	if (GlobalPdata.SDL_Data[joy].held_buttons[button])
		return 1;
	else
		return 0;
}

long WINAPI DLLExport HoldButtonLeftShoulder(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long button = SDL_CONTROLLER_BUTTON_LEFTSHOULDER;
	if (GlobalPdata.SDL_Data[joy].held_buttons[button])
		return 1;
	else
		return 0;
}

long WINAPI DLLExport HoldButtonRightShoulder(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long button = SDL_CONTROLLER_BUTTON_RIGHTSHOULDER;
	if (GlobalPdata.SDL_Data[joy].held_buttons[button])
		return 1;
	else
		return 0;
}

long WINAPI DLLExport HoldDpadUp(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	// For GameController, return the D-Pad button value
	if (GlobalPdata.SDL_Data[joy].controller != nullptr)
		return (long)(GlobalPdata.SDL_Data[joy].held_buttons[SDL_CONTROLLER_BUTTON_DPAD_UP]);
	// For Joystick, return the Hat value 
	else
		return (long)(GlobalPdata.SDL_Data[joy].hat[0] & SDL_HAT_UP);
}

long WINAPI DLLExport HoldDpadDown(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	// For GameController, return the D-Pad button value
	if (GlobalPdata.SDL_Data[joy].controller != nullptr)
		return (long)(GlobalPdata.SDL_Data[joy].held_buttons[SDL_CONTROLLER_BUTTON_DPAD_DOWN]);
	// For Joystick, return the Hat value 
	else
		return (long)(GlobalPdata.SDL_Data[joy].hat[0] & SDL_HAT_DOWN);
}

long WINAPI DLLExport HoldDpadLeft(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	// For GameController, return the D-Pad button value
	if (GlobalPdata.SDL_Data[joy].controller != nullptr)
		return (long)(GlobalPdata.SDL_Data[joy].held_buttons[SDL_CONTROLLER_BUTTON_DPAD_LEFT]);
	// For Joystick, return the Hat value 
	else
		return (long)(GlobalPdata.SDL_Data[joy].hat[0] & SDL_HAT_LEFT);
}

long WINAPI DLLExport HoldDpadRight(LPRDATA rdPtr, long param1, long param2)
{
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	// For GameController, return the D-Pad button value
	if (GlobalPdata.SDL_Data[joy].controller != nullptr)
		return (long)(GlobalPdata.SDL_Data[joy].held_buttons[SDL_CONTROLLER_BUTTON_DPAD_RIGHT]);
	// For Joystick, return the Hat value 
	else
		return (long)(GlobalPdata.SDL_Data[joy].hat[0] & SDL_HAT_RIGHT);
}

long WINAPI DLLExport DeviceHasLED(LPRDATA rdPtr, long param1, long param2)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	if (GlobalPdata.SDL_Data[joy].controller != nullptr)
		return (long)(SDL_GameControllerHasLED(GlobalPdata.SDL_Data[joy].controller));
	else
		return (long)(SDL_JoystickHasLED(GlobalPdata.SDL_Data[joy].joystick));
}

long WINAPI DLLExport SDLEventJoystickBallMotion(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long ball = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	ball = max(0, min(MAX_BALL - 1, ball));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	for (int i = 0; i < MAX_EVENT; i++)
	{
		// If there are no events left, the condition is false
		if (GlobalPdata.SDL_Events[i].common.type == SDL_FIRSTEVENT)
			return 0;
		// Event ID, joystick instance ID and axis ID must match + deadzone check
		if (GlobalPdata.SDL_Events[i].common.type == SDL_JOYBALLMOTION)
		{
			if (GlobalPdata.SDL_Events[i].jball.which == GlobalPdata.SDL_Data[joy].joy_id &&
				GlobalPdata.SDL_Events[i].jball.ball == ball)
				return 1;
		}
	}
	return 0;
}

long WINAPI DLLExport SDLEventTouchpad(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long touchpad = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	touchpad = max(0, min(MAX_TOUCHPAD - 1, touchpad));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	for (int i = 0; i < MAX_EVENT; i++)
	{
		// If there are no events left, the condition is false
		if (GlobalPdata.SDL_Events[i].common.type == SDL_FIRSTEVENT)
			return 0;
		// Device ID and touchpad ID must match
		if (GlobalPdata.SDL_Events[i].ctouchpad.which == GlobalPdata.SDL_Data[joy].joy_id && GlobalPdata.SDL_Events[i].ctouchpad.touchpad == touchpad)
		{
			// Touchpad events (pressed)
			if (state == SDL_PRESSED && GlobalPdata.SDL_Events[i].common.type == SDL_CONTROLLERTOUCHPADDOWN)
				return 1;
			// Touchpad events (released)
			if (state == SDL_RELEASED && GlobalPdata.SDL_Events[i].common.type == SDL_CONTROLLERTOUCHPADUP)
				return 1;
		}
	}
	return 0;
}

long WINAPI DLLExport SDLEventFinger(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long touchpad = CNC_GetIntParameter(rdPtr);
	long finger = CNC_GetIntParameter(rdPtr);
	long state = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	touchpad = max(0, min(MAX_TOUCHPAD - 1, touchpad));
	finger = max(0, min(MAX_FINGER - 1, finger));
	state = max(SDL_RELEASED, min(SDL_PRESSED, state));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	for (int i = 0; i < MAX_EVENT; i++)
	{
		// If there are no events left, the condition is false
		if (GlobalPdata.SDL_Events[i].common.type == SDL_FIRSTEVENT)
			return 0;
		// Device ID, touchpad ID and finger ID must match
		if (GlobalPdata.SDL_Events[i].ctouchpad.which == GlobalPdata.SDL_Data[joy].joy_id && 
			GlobalPdata.SDL_Events[i].ctouchpad.touchpad == touchpad &&
			GlobalPdata.SDL_Events[i].ctouchpad.finger == finger)
		{
			// Touchpad events (pressed)
			if (state == SDL_PRESSED && GlobalPdata.SDL_Events[i].common.type == SDL_CONTROLLERTOUCHPADDOWN)
				return 1;
			// Touchpad events (released)
			if (state == SDL_RELEASED && GlobalPdata.SDL_Events[i].common.type == SDL_CONTROLLERTOUCHPADUP)
				return 1;
		}
	}
	return 0;
}

long WINAPI DLLExport SDLEventTouchpadMotion(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long touchpad = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	touchpad = max(0, min(MAX_TOUCHPAD - 1, touchpad));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	for (int i = 0; i < MAX_EVENT; i++)
	{
		// If there are no events left, the condition is false
		if (GlobalPdata.SDL_Events[i].common.type == SDL_FIRSTEVENT)
			return false;
		// Event ID must match
		if (GlobalPdata.SDL_Events[i].common.type == SDL_CONTROLLERTOUCHPADMOTION)
			// Device and touchpad must match
			if (GlobalPdata.SDL_Events[i].ctouchpad.which == GlobalPdata.SDL_Data[joy].joy_id && GlobalPdata.SDL_Events[i].ctouchpad.touchpad == touchpad)
				return 1;
	}
	return 0;
}

long WINAPI DLLExport SDLEventFingerMotion(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	long touchpad = CNC_GetIntParameter(rdPtr);
	long finger = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	touchpad = max(0, min(MAX_TOUCHPAD - 1, touchpad));
	finger = max(0, min(MAX_FINGER - 1, finger));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	for (int i = 0; i < MAX_EVENT; i++)
	{
		// If there are no events left, the condition is false
		if (GlobalPdata.SDL_Events[i].common.type == SDL_FIRSTEVENT)
			return 0;
		// Event ID must match
		if (GlobalPdata.SDL_Events[i].common.type == SDL_CONTROLLERTOUCHPADMOTION)
			// Device and touchpad must match
			if (GlobalPdata.SDL_Events[i].ctouchpad.which == GlobalPdata.SDL_Data[joy].joy_id && GlobalPdata.SDL_Events[i].ctouchpad.touchpad == touchpad)
				// Finger ID must match
				if (GlobalPdata.SDL_Events[i].ctouchpad.finger == finger)
					return 1;
	}
	return 0;
}

long WINAPI DLLExport SDLEventBattery(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	for (int i = 0; i < MAX_EVENT; i++)
	{
		// If there are no events left, the condition is false
		if (GlobalPdata.SDL_Events[i].common.type == SDL_FIRSTEVENT)
			return 0;
		// Event ID must match
		if (GlobalPdata.SDL_Events[i].common.type == SDL_JOYBATTERYUPDATED &&
			GlobalPdata.SDL_Events[i].jbattery.which == GlobalPdata.SDL_Data[joy].joy_id)
			return 1;
	}
	return 0;
}

long WINAPI DLLExport HasTriggerRumble(LPRDATA rdPtr, long param1, long param2)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	if (GlobalPdata.SDL_Data[joy].controller != nullptr)
		return SDL_GameControllerHasRumbleTriggers(GlobalPdata.SDL_Data[joy].controller);
	else if (GlobalPdata.SDL_Data[joy].joystick != nullptr)
		return SDL_JoystickHasRumbleTriggers(GlobalPdata.SDL_Data[joy].joystick);
	return 0;
}

long WINAPI DLLExport HasAccelerator(LPRDATA rdPtr, long param1, long param2)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long accel = max(0, min(2, param2));
	if (GlobalPdata.SDL_Data[joy].controller != nullptr)
	{
		switch (accel)
		{
		case 0:
			return SDL_GameControllerHasSensor(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_ACCEL);
		case 1:
			return SDL_GameControllerHasSensor(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_ACCEL_L);
		case 2:
			return SDL_GameControllerHasSensor(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_ACCEL_R);
		}
	}
	return 0;
}

long WINAPI DLLExport AcceleratorEnabled(LPRDATA rdPtr, long param1, long param2)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long accel = max(0, min(2, param2));
	if (GlobalPdata.SDL_Data[joy].controller != nullptr)
	{
		switch (accel)
		{
		case 0:
			return SDL_GameControllerIsSensorEnabled(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_ACCEL);
		case 1:
			return SDL_GameControllerIsSensorEnabled(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_ACCEL_L);
		case 2:
			return SDL_GameControllerIsSensorEnabled(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_ACCEL_R);
		}
	}
	return 0;
}

long WINAPI DLLExport HasGyroscope(LPRDATA rdPtr, long param1, long param2)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long gyro = max(0, min(2, param2));
	if (GlobalPdata.SDL_Data[joy].controller != nullptr)
	{
		switch (gyro)
		{
		case 0:
			return SDL_GameControllerHasSensor(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_GYRO);
		case 1:
			return SDL_GameControllerHasSensor(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_GYRO_L);
		case 2:
			return SDL_GameControllerHasSensor(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_GYRO_R);
		}
	}
	return 0;
}

long WINAPI DLLExport GyroscopeEnabled(LPRDATA rdPtr, long param1, long param2)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = max(0, min(MAX_DEVICE - 1, param1));
	long gyro = max(0, min(2, param2));
	if (GlobalPdata.SDL_Data[joy].controller != nullptr)
	{
		switch (gyro)
		{
		case 0:
			return SDL_GameControllerHasSensor(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_GYRO);
		case 1:
			return SDL_GameControllerHasSensor(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_GYRO_L);
		case 2:
			return SDL_GameControllerHasSensor(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_GYRO_R);
		}
	}
	return 0;
}

long WINAPI DLLExport SDLEventRemapped(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	for (int i = 0; i < MAX_EVENT; i++)
	{
		// If there are no events left, the condition is false
		if (GlobalPdata.SDL_Events[i].common.type == SDL_FIRSTEVENT)
			return 0;
		// Event ID must match
		if (GlobalPdata.SDL_Events[i].common.type == SDL_CONTROLLERDEVICEREMAPPED &&
			GlobalPdata.SDL_Events[i].cdevice.which == SDL_JoystickInstanceID(GlobalPdata.SDL_Data[joy].joystick))
			return 1;
	}
	return 0;
}

long WINAPI DLLExport SDLEventAnyRemapped(LPRDATA rdPtr, long param1, long param2)
{
	for (int i = 0; i < MAX_EVENT; i++)
	{
		// If there are no events left, the condition is false
		if (GlobalPdata.SDL_Events[i].common.type == SDL_FIRSTEVENT)
			return 0;
		// Event ID must match
		if (GlobalPdata.SDL_Events[i].common.type == SDL_CONTROLLERDEVICEREMAPPED)
			return 1;
	}
	return 0;
}

long WINAPI DLLExport SDLEventSensor(LPRDATA rdPtr, long param1, long param2)
{
	long joy = CNC_GetIntParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	for (int i = 0; i < MAX_EVENT; i++)
	{
		// If there are no events left, the condition is false
		if (GlobalPdata.SDL_Events[i].common.type == SDL_FIRSTEVENT)
			return 0;
		// Event ID must match
		if (GlobalPdata.SDL_Events[i].common.type == SDL_CONTROLLERSENSORUPDATE )
			return 1;
	}
	return 0;
}

// ============================================================================
//
// ACTIONS ROUTINES
// 
// ============================================================================

short WINAPI DLLExport LoadDB(LPRDATA rdPtr, long param1, long param2)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	LPCTSTR pFilename = (LPCTSTR)param1;
	// Add controller mappings
	SDL_GameControllerAddMappingsFromFile(WcharToChar(pFilename, lstrlen(pFilename), 0));
	// Reopen all joysticks
	for (int i = 0; i < MAX_DEVICE; i++)
		ReloadMapping(i);
	return 0;
}

short WINAPI DLLExport Rumble(LPRDATA rdPtr, long param1, long param2)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = CNC_GetParameter(rdPtr);
	long low = CNC_GetParameter(rdPtr);
	long high = CNC_GetParameter(rdPtr);
	long duration = CNC_GetParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	low = max(0, min(65535, low));
	high = max(0, min(65535, high));
	duration = max(0, min(3600000, duration)); // One hour rumble!
	if (GlobalPdata.SDL_Data[joy].controller != nullptr)
		SDL_GameControllerRumble(GlobalPdata.SDL_Data[joy].controller, (Uint16)low, (Uint16)high, duration);
	else if (GlobalPdata.SDL_Data[joy].joystick != nullptr)
		SDL_JoystickRumble(GlobalPdata.SDL_Data[joy].joystick, (Uint16)low, (Uint16)high, duration);
	return 0;
}

short WINAPI DLLExport SetLEDColor(LPRDATA rdPtr, long param1, long param2)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = CNC_GetParameter(rdPtr);
	long color = CNC_GetParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	color = max(0, min(16777215, color)); // 0xFFFFFF is maximum
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	if (!SDL_JoystickHasLED(GlobalPdata.SDL_Data[joy].joystick))
		return 0;
	Uint8 r = color & 0xFF;
	Uint8 g = (color >> 8) & 0xFF;
	Uint8 b = (color >> 16) & 0xFF;	
	SDL_JoystickSetLED(GlobalPdata.SDL_Data[joy].joystick, r, g, b);
	return 0;
}

short WINAPI DLLExport AddMapping(LPRDATA rdPtr, long param1, long param2)
{
	if (GlobalPdata.DisableGameController)
		return 0;
	long guid = CNC_GetStringParameter(rdPtr);
	long name = CNC_GetStringParameter(rdPtr);
	long mapping = CNC_GetStringParameter(rdPtr);
	LPCTSTR pGuid = (LPCTSTR)guid;
	LPCTSTR pName = (LPCTSTR)name;
	LPCTSTR pMapping = (LPCTSTR)mapping;
	// Allocate string
	LPTSTR mappingFull = (LPTSTR)callRunTimeFunction(rdPtr, RFUNCTION_GETSTRINGSPACE_EX, 0, 512 * sizeof(TCHAR));
	if (mappingFull == NULL)
		return 0;
	wsprintf(mappingFull, L"%s,%s,%s", pGuid, pName, pMapping);
	SDL_GameControllerAddMapping(WcharToChar(mappingFull, lstrlen(mappingFull), 0));
	// Find the joystick by GUID and reopen it
	for (int i = 0; i < MAX_DEVICE; i++)
	{
		if (!GlobalPdata.SDL_Data[i].connected)
			continue;
		SDL_JoystickGUID guid_new = SDL_GUIDFromString(WcharToChar(pGuid, lstrlen(pGuid), 0));
		bool different = false;
		for (int d = 0; d < 16; d++)
		{
			if (GlobalPdata.SDL_Data[i].guid.data[d] != guid_new.data[d])
				different = true;
		}
		if (!different)
			ReloadMapping(i);
	}
	return 0;
}

short WINAPI DLLExport AddDeviceMapping(LPRDATA rdPtr, long param1, long param2)
{
	if (GlobalPdata.DisableGameController)
		return 0;
	long joy = CNC_GetIntParameter(rdPtr);
	long mapping = CNC_GetStringParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	LPCTSTR pMapping = (LPCTSTR)mapping;
	// Allocate string
	LPTSTR mappingFull = (LPTSTR)callRunTimeFunction(rdPtr, RFUNCTION_GETSTRINGSPACE_EX, 0, 512 * sizeof(TCHAR));
	if (mappingFull == NULL)
		return 0;
	wsprintf(mappingFull, L"%s,%s,%s", GlobalPdata.SDL_Data[joy].guid, GlobalPdata.SDL_Data[joy].name, pMapping);
	SDL_GameControllerAddMapping(WcharToChar(mappingFull, lstrlen(mappingFull), 0));
	ReloadMapping(joy);
	return 0;
}

short WINAPI DLLExport RumbleTriggers(LPRDATA rdPtr, long param1, long param2)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = CNC_GetParameter(rdPtr);
	long low = CNC_GetParameter(rdPtr);
	long high = CNC_GetParameter(rdPtr);
	long duration = CNC_GetParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	low = max(0, min(65535, low));
	high = max(0, min(65535, high));
	duration = max(0, min(3600000, duration)); // One hour rumble!
	if (GlobalPdata.SDL_Data[joy].controller != nullptr)
		SDL_GameControllerRumbleTriggers(GlobalPdata.SDL_Data[joy].controller, (Uint16)low, (Uint16)high, duration);
	else if (GlobalPdata.SDL_Data[joy].joystick != nullptr)
		SDL_JoystickRumbleTriggers(GlobalPdata.SDL_Data[joy].joystick, (Uint16)low, (Uint16)high, duration);
	return 0;
}

short WINAPI DLLExport SetPlayer(LPRDATA rdPtr, long param1, long param2)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = CNC_GetParameter(rdPtr);
	long player = CNC_GetParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	player = max(0, min(MAX_SDLPLAYER, player));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	if (GlobalPdata.SDL_Data[joy].controller != nullptr)
		SDL_GameControllerSetPlayerIndex(GlobalPdata.SDL_Data[joy].controller, player);
	else if (GlobalPdata.SDL_Data[joy].joystick != nullptr)
		SDL_JoystickSetPlayerIndex(GlobalPdata.SDL_Data[joy].joystick, player);
	return 0;
}

short WINAPI DLLExport SetAccelerometerState(LPRDATA rdPtr, long param1, long param2)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = CNC_GetParameter(rdPtr);
	long accel = CNC_GetParameter(rdPtr);
	long state = CNC_GetParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	accel = max(0, min(MAX_SENSOR, accel));
	state = (max(SDL_DISABLE, min(SDL_ENABLE, state)));
	if (!GlobalPdata.SDL_Data[joy].connected || GlobalPdata.SDL_Data[joy].controller == nullptr)
		return 0;
	switch (accel)
	{
	case 0:
		SDL_GameControllerSetSensorEnabled(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_ACCEL, (SDL_bool)state);
		break;
	case 1:
		SDL_GameControllerSetSensorEnabled(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_ACCEL_L, (SDL_bool)state);
		break;
	case 2:
		SDL_GameControllerSetSensorEnabled(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_ACCEL_R, (SDL_bool)state);
		break;
	}
	return 0;
}

short WINAPI DLLExport SetGyroscopeState(LPRDATA rdPtr, long param1, long param2)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = CNC_GetParameter(rdPtr);
	long gyro = CNC_GetParameter(rdPtr);
	long state = CNC_GetParameter(rdPtr);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	gyro = max(0, min(MAX_SENSOR, gyro));
	state = (max(SDL_DISABLE, min(SDL_ENABLE, state)));
	if (!GlobalPdata.SDL_Data[joy].connected || GlobalPdata.SDL_Data[joy].controller == nullptr)
		return 0;
	switch (gyro)
	{
	case 0:
		SDL_GameControllerSetSensorEnabled(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_GYRO, (SDL_bool)state);
		break;
	case 1:
		SDL_GameControllerSetSensorEnabled(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_GYRO_L, (SDL_bool)state);
		break;
	case 2:
		SDL_GameControllerSetSensorEnabled(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_GYRO_R, (SDL_bool)state);
		break;
	}
	return 0;
}

short WINAPI DLLExport PollInput(LPRDATA rdPtr, long param1, long param2)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	SDL_JoystickUpdate();
	if (!GlobalPdata.DisableGameController)
		SDL_GameControllerUpdate();
	for (int j = 0; j < MAX_DEVICE; j++)
		UpdateInput(j);
	return 0;
}

// ============================================================================
//
// EXPRESSIONS ROUTINES
// 
// ============================================================================

long WINAPI DLLExport GetAxis(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long axis = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	axis = max(0, min(MAX_AXIS - 1, axis));
	return (long)(GlobalPdata.SDL_Data[joy].axis[axis]);
}

long WINAPI DLLExport GetButton(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long button = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	button = max(0, min(MAX_BUTTON - 1, button));
	return (long)(GlobalPdata.SDL_Data[joy].held_buttons[button]);
}

long WINAPI DLLExport GetHat(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long hat = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	hat = max(0, min(MAX_HAT, hat));
	return (long)(GlobalPdata.SDL_Data[joy].hat[hat]);
}

long WINAPI DLLExport GetBallX(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long ball = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	ball = max(0, min(MAX_BALL - 1, ball));
	return (long)(GlobalPdata.SDL_Data[joy].ball_x[ball]);
}

long WINAPI DLLExport GetBallY(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long ball = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	ball = max(0, min(MAX_BALL - 1, ball));
	return (long)(GlobalPdata.SDL_Data[joy].ball_y[ball]);
}

long WINAPI DLLExport NumAxes(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	return (long)(GlobalPdata.SDL_Data[joy].num_axes);
}

long WINAPI DLLExport NumButtons(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	return (long)(GlobalPdata.SDL_Data[joy].num_buttons);
}

long WINAPI DLLExport NumHats(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	return (long)(GlobalPdata.SDL_Data[joy].num_hats);
}

long WINAPI DLLExport NumBalls(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	return (long)(GlobalPdata.SDL_Data[joy].num_balls);
}

long WINAPI DLLExport NumJoysticks(LPRDATA rdPtr, long param1)
{
	return GlobalPdata.NumDevices;
}

long WINAPI DLLExport GetDeviceGUID(LPRDATA rdPtr, long param1)
{
	LPTSTR pszGuidSLpt = (LPTSTR)callRunTimeFunction(rdPtr, RFUNCTION_GETSTRINGSPACE_EX, 0, 33 * sizeof(TCHAR));
	char* pszGuidChar = (char*)callRunTimeFunction(rdPtr, RFUNCTION_GETSTRINGSPACE_EX, 0, 33 * sizeof(char));
	if (GlobalPdata.SDLInitState)
	{
		long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
		joy = max(0, min(MAX_DEVICE - 1, joy));
		if (pszGuidChar != NULL && pszGuidSLpt != NULL)
		{
			SDL_JoystickGetGUIDString(GlobalPdata.SDL_Data[joy].guid, pszGuidChar, 33);
			mbstowcs(pszGuidSLpt, pszGuidChar, 33);
		}
	}
	rdPtr->rHo.hoFlags |= HOF_STRING;
	return (long)pszGuidSLpt;
}

long WINAPI DLLExport GetDeviceName(LPRDATA rdPtr, long param1)
{
	LPTSTR pszName = (LPTSTR)callRunTimeFunction(rdPtr, RFUNCTION_GETSTRINGSPACE_EX, 0, 512 * sizeof(TCHAR));
	if (GlobalPdata.SDLInitState)
	{
		long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
		joy = max(0, min(MAX_DEVICE - 1, joy));
		if (pszName != NULL)
		{
			wsprintf(pszName, L"%s", GlobalPdata.SDL_Data[joy].name);
		}
	}
	rdPtr->rHo.hoFlags |= HOF_STRING;
	return (long)pszName;
}

long WINAPI DLLExport GetButtonsHeld(LPRDATA rdPtr, long param1)
{
	LPTSTR pszName = (LPTSTR)callRunTimeFunction(rdPtr, RFUNCTION_GETSTRINGSPACE_EX, 0, 16 * sizeof(TCHAR));
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	int buttons = 0;
	if (pszName != NULL)
	{
		for (int i = 0; i < MAX_BUTTON; i++)
		{
			if (GlobalPdata.SDL_Data[joy].held_buttons[i]) buttons |= (1 << i);
		}
		wsprintf(pszName, L"%08X", buttons);
	}
	rdPtr->rHo.hoFlags |= HOF_STRING;
	return (long)pszName;
}

long WINAPI DLLExport HeldButton(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long index = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	index = max(0, (min(MAX_BUTTON - 1, index)));
	return GlobalPdata.SDL_Data[joy].currentheld[index];
}

long WINAPI DLLExport LastButtonPressed(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_LONG);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	return GlobalPdata.SDL_Data[joy].lastpressed;
}

long WINAPI DLLExport LastButtonReleased(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_LONG);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	return GlobalPdata.SDL_Data[joy].lastreleased;
}

long WINAPI DLLExport GetAxisInitial(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long axis = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	axis = max(0, min(MAX_AXIS - 1, axis));
	return (long)(GlobalPdata.SDL_Data[joy].axis_initial[axis]);
}

long WINAPI DLLExport NumTouchpads(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	return (long)(GlobalPdata.SDL_Data[joy].num_touchpads);
}

long WINAPI DLLExport NumFingers(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long touchpad = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	touchpad = max(0, min(MAX_TOUCHPAD - 1, touchpad));
	return (long)(GlobalPdata.SDL_Data[joy].touchpads[touchpad].num_fingers);
}

long WINAPI DLLExport GetBatteryLevel(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	switch (GlobalPdata.SDL_Data[joy].battery)
	{
	case SDL_JOYSTICK_POWER_EMPTY:
		return (long)0;
	case SDL_JOYSTICK_POWER_LOW:
		return (long)20;
	case SDL_JOYSTICK_POWER_MEDIUM:
		return (long)70;
	case SDL_JOYSTICK_POWER_FULL:
	case SDL_JOYSTICK_POWER_WIRED:
	case SDL_JOYSTICK_POWER_UNKNOWN:
	default:
		return (long)100;
	}
}

long WINAPI DLLExport GetFingerState(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long touchpad = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	long finger = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	touchpad = max(0, min(MAX_TOUCHPAD - 1, touchpad));
	finger = max(0, min(MAX_FINGER - 1, finger));
	return (long)(GlobalPdata.SDL_Data[joy].touchpads[touchpad].finger[finger].state);
}

long WINAPI DLLExport GetFingerX(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long touchpad = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	long finger = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	touchpad = max(0, min(MAX_TOUCHPAD - 1, touchpad));
	finger = max(0, min(MAX_FINGER - 1, finger));
	rdPtr->rHo.hoFlags |= HOF_FLOAT;
	float fResult = GlobalPdata.SDL_Data[joy].touchpads[touchpad].finger[finger].x;
	return *((long*)&fResult);
}

long WINAPI DLLExport GetFingerY(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long touchpad = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	long finger = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	touchpad = max(0, min(MAX_TOUCHPAD - 1, touchpad));
	finger = max(0, min(MAX_FINGER - 1, finger));
	rdPtr->rHo.hoFlags |= HOF_FLOAT;
	float fResult = GlobalPdata.SDL_Data[joy].touchpads[touchpad].finger[finger].y;
	return *((long*)&fResult);
}

long WINAPI DLLExport GetFingerPressure(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long touchpad = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	long finger = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	touchpad = max(0, min(MAX_TOUCHPAD - 1, touchpad));
	finger = max(0, min(MAX_FINGER - 1, finger));
	rdPtr->rHo.hoFlags |= HOF_FLOAT;
	float fResult = GlobalPdata.SDL_Data[joy].touchpads[touchpad].finger[finger].pressure;
	return *((long*)&fResult);
}

long WINAPI DLLExport JoystickType(LPRDATA rdPtr, long param1)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	return (long)SDL_JoystickGetType(GlobalPdata.SDL_Data[joy].joystick);
}

long WINAPI DLLExport ControllerType(LPRDATA rdPtr, long param1)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	return (long)(GlobalPdata.SDL_Data[joy].type_controller);
}

long WINAPI DLLExport GetPlayer(LPRDATA rdPtr, long param1)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	if (GlobalPdata.SDL_Data[joy].controller != nullptr)
		return (long)SDL_GameControllerGetPlayerIndex(GlobalPdata.SDL_Data[joy].controller);
	else if (GlobalPdata.SDL_Data[joy].joystick != nullptr)
		return (long)SDL_JoystickGetPlayerIndex(GlobalPdata.SDL_Data[joy].joystick);
	return -1;
}

long WINAPI DLLExport DeviceVendor(LPRDATA rdPtr, long param1)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	return (long)SDL_JoystickGetVendor(GlobalPdata.SDL_Data[joy].joystick);
}

long WINAPI DLLExport DeviceProduct(LPRDATA rdPtr, long param1)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	return (long)SDL_JoystickGetProduct(GlobalPdata.SDL_Data[joy].joystick);
}

long WINAPI DLLExport DeviceProductVersion(LPRDATA rdPtr, long param1)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	return (long)SDL_JoystickGetProductVersion(GlobalPdata.SDL_Data[joy].joystick);
}

long WINAPI DLLExport DeviceFirmware(LPRDATA rdPtr, long param1)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	return (long)SDL_JoystickGetFirmwareVersion(GlobalPdata.SDL_Data[joy].joystick);
}

long WINAPI DLLExport DeviceSerial(LPRDATA rdPtr, long param1)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	if (!GlobalPdata.SDL_Data[joy].connected)
		return 0;
	LPTSTR pszSerial = (LPTSTR)callRunTimeFunction(rdPtr, RFUNCTION_GETSTRINGSPACE_EX, 0, 128 * sizeof(TCHAR));
	if (pszSerial != NULL)
	{
		wsprintf(pszSerial, L"%s", SDL_JoystickGetSerial(GlobalPdata.SDL_Data[joy].joystick));
	}
	rdPtr->rHo.hoFlags |= HOF_STRING;
	return (long)pszSerial;
}

long WINAPI DLLExport GetAcceleratorState(LPRDATA rdPtr, long param1)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long accelerator = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	accelerator = max(0, min(MAX_SENSOR, accelerator));
	switch (accelerator)
	{
	case 0:
		return (long)(SDL_GameControllerIsSensorEnabled(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_ACCEL));
	case 1:
		return (long)(SDL_GameControllerIsSensorEnabled(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_ACCEL_L));
	case 2:
		return (long)(SDL_GameControllerIsSensorEnabled(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_ACCEL_R));
	}
	return 0;
}

long WINAPI DLLExport GetAcceleratorX(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long accelerator = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	accelerator = max(0, min(MAX_SENSOR, accelerator));
	float fResult = GetSensorData(joy, false, accelerator, 0);
	rdPtr->rHo.hoFlags |= HOF_FLOAT;
	return *((long*)&fResult);
}

long WINAPI DLLExport GetAcceleratorY(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long accelerator = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	accelerator = max(0, min(MAX_SENSOR, accelerator));
	float fResult = GetSensorData(joy, false, accelerator, 1);
	rdPtr->rHo.hoFlags |= HOF_FLOAT;
	return *((long*)&fResult);
}

long WINAPI DLLExport GetAcceleratorZ(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long accelerator = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	accelerator = max(0, min(MAX_SENSOR, accelerator));
	float fResult = GetSensorData(joy, false, accelerator, 2);
	rdPtr->rHo.hoFlags |= HOF_FLOAT;
	return *((long*)&fResult);
}

long WINAPI DLLExport GetGyroscopeState(LPRDATA rdPtr, long param1)
{
	if (!GlobalPdata.SDLInitState)
		return 0;
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long gyro = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	gyro = max(0, min(MAX_SENSOR, gyro));
	switch (gyro)
	{
	case 0:
		return (long)(SDL_GameControllerIsSensorEnabled(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_GYRO));
	case 1:
		return (long)(SDL_GameControllerIsSensorEnabled(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_GYRO_L));
	case 2:
		return (long)(SDL_GameControllerIsSensorEnabled(GlobalPdata.SDL_Data[joy].controller, SDL_SENSOR_GYRO_R));
	}
	return 0;
}

long WINAPI DLLExport GetGyroscopeX(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long gyro = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	gyro = max(0, min(MAX_SENSOR, gyro));
	float fResult = GetSensorData(joy, true, gyro, 0);
	rdPtr->rHo.hoFlags |= HOF_FLOAT;
	return *((long*)&fResult);
}

long WINAPI DLLExport GetGyroscopeY(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long gyro = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	gyro = max(0, min(MAX_SENSOR, gyro));
	float fResult = GetSensorData(joy, true, gyro, 1);
	rdPtr->rHo.hoFlags |= HOF_FLOAT;
	return *((long*)&fResult);
}

long WINAPI DLLExport GetGyroscopeZ(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long gyro = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	gyro = max(0, min(MAX_SENSOR, gyro));
	float fResult = GetSensorData(joy, true, gyro, 2);
	rdPtr->rHo.hoFlags |= HOF_FLOAT;
	return *((long*)&fResult);
}

long WINAPI DLLExport GetBindStringDevice(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	LPTSTR pszBind;
	if (GlobalPdata.SDLInitState && GlobalPdata.SDL_Data[joy].connected)
	{
		char* binding = SDL_GameControllerMappingForGUID(GlobalPdata.SDL_Data[joy].guid);
		if (binding != NULL)
		{
			pszBind = (LPTSTR)callRunTimeFunction(rdPtr, RFUNCTION_GETSTRINGSPACE_EX, 0, strlen(binding) * sizeof(TCHAR));
			if (pszBind != NULL)
				mbstowcs(pszBind, binding, strlen(binding));
			SDL_free(binding);
		}
	}
	rdPtr->rHo.hoFlags |= HOF_STRING;
	return (long)pszBind;
}

long WINAPI DLLExport GetBindStringAxis(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long axis = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	axis = max(0, min(MAX_AXIS - 1, axis));
	if (!GlobalPdata.SDLInitState || !GlobalPdata.SDL_Data[joy].connected)
		return 0;
	LPTSTR pszBind = (LPTSTR)callRunTimeFunction(rdPtr, RFUNCTION_GETSTRINGSPACE_EX, 0, 32 * sizeof(TCHAR));
	SDL_GameControllerButtonBind bind = SDL_GameControllerGetBindForAxis(GlobalPdata.SDL_Data[joy].controller, (SDL_GameControllerAxis)axis);
	if (pszBind != NULL)
	{
		switch (bind.bindType)
		{
		case SDL_CONTROLLER_BINDTYPE_NONE:
		default:
			wsprintf(pszBind, L"");
			break;
		case SDL_CONTROLLER_BINDTYPE_BUTTON:
			wsprintf(pszBind, L"b%d", bind.value.button);
			break;
		case SDL_CONTROLLER_BINDTYPE_AXIS:
			wsprintf(pszBind, L"a%d", bind.value.axis);
			break;
		case SDL_CONTROLLER_BINDTYPE_HAT:
			wsprintf(pszBind, L"h%d.%d", bind.value.hat.hat, bind.value.hat.hat_mask);
			break;
		}
	}
	rdPtr->rHo.hoFlags |= HOF_STRING;
	return (long)pszBind;
}

long WINAPI DLLExport GetBindStringButton(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long button = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	button = max(0, min(MAX_BUTTON - 1, button));
	if (!GlobalPdata.SDLInitState || !GlobalPdata.SDL_Data[joy].connected)
		return 0;
	LPTSTR pszBind = (LPTSTR)callRunTimeFunction(rdPtr, RFUNCTION_GETSTRINGSPACE_EX, 0, 32 * sizeof(TCHAR));
	SDL_GameControllerButtonBind bind = SDL_GameControllerGetBindForButton(GlobalPdata.SDL_Data[joy].controller, (SDL_GameControllerButton)button);
	if (pszBind != NULL)
	{
		switch (bind.bindType)
		{
		case SDL_CONTROLLER_BINDTYPE_NONE:
		default:
			wsprintf(pszBind, L"");
			break;
		case SDL_CONTROLLER_BINDTYPE_BUTTON:
			wsprintf(pszBind, L"b%d", bind.value.button);
			break;
		case SDL_CONTROLLER_BINDTYPE_AXIS:
			wsprintf(pszBind, L"a%d", bind.value.axis);
			break;
		case SDL_CONTROLLER_BINDTYPE_HAT:
			wsprintf(pszBind, L"h%d.%d", bind.value.hat.hat, bind.value.hat.hat_mask);
			break;
		}
	}
	rdPtr->rHo.hoFlags |= HOF_STRING;
	return (long)pszBind;
}

long WINAPI DLLExport GetBindTypeAxis(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long axis = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	axis = max(0, min(MAX_AXIS - 1, axis));
	if (!GlobalPdata.SDLInitState || !GlobalPdata.SDL_Data[joy].connected)
		return 0;
	SDL_GameControllerButtonBind bind = SDL_GameControllerGetBindForAxis(GlobalPdata.SDL_Data[joy].controller, (SDL_GameControllerAxis)axis);
	return (long)bind.bindType;
}

long WINAPI DLLExport GetBindValueAxis(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long axis = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	axis = max(0, min(MAX_AXIS - 1, axis));
	if (!GlobalPdata.SDLInitState || !GlobalPdata.SDL_Data[joy].connected)
		return 0;
	SDL_GameControllerButtonBind bind = SDL_GameControllerGetBindForAxis(GlobalPdata.SDL_Data[joy].controller, (SDL_GameControllerAxis)axis);
	switch (bind.bindType)
	{
		case SDL_CONTROLLER_BINDTYPE_NONE:
		default:
			return 0;
		case SDL_CONTROLLER_BINDTYPE_BUTTON:
			return (long)bind.value.button;
		case SDL_CONTROLLER_BINDTYPE_AXIS:
			return (long)bind.value.axis;
		case SDL_CONTROLLER_BINDTYPE_HAT:
			return (long)(bind.value.hat.hat * 100 + bind.value.hat.hat_mask);
	}
}

long WINAPI DLLExport GetBindTypeButton(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long button = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	button = max(0, min(MAX_BUTTON - 1, button));
	if (!GlobalPdata.SDLInitState || !GlobalPdata.SDL_Data[joy].connected)
		return 0;
	SDL_GameControllerButtonBind bind = SDL_GameControllerGetBindForButton(GlobalPdata.SDL_Data[joy].controller, (SDL_GameControllerButton)button);
	return (long)bind.bindType;
}

long WINAPI DLLExport GetBindValueButton(LPRDATA rdPtr, long param1)
{
	long joy = CNC_GetFirstExpressionParameter(rdPtr, param1, TYPE_INT);
	long button = CNC_GetNextExpressionParameter(rdPtr, param1, TYPE_INT);
	joy = max(0, min(MAX_DEVICE - 1, joy));
	button = max(0, min(MAX_BUTTON - 1, button));
	if (!GlobalPdata.SDLInitState || !GlobalPdata.SDL_Data[joy].connected)
		return 0;
	SDL_GameControllerButtonBind bind = SDL_GameControllerGetBindForButton(GlobalPdata.SDL_Data[joy].controller, (SDL_GameControllerButton)button);
	switch (bind.bindType)
	{
	case SDL_CONTROLLER_BINDTYPE_NONE:
	default:
		return 0;
	case SDL_CONTROLLER_BINDTYPE_BUTTON:
		return (long)bind.value.button;
	case SDL_CONTROLLER_BINDTYPE_AXIS:
		return (long)bind.value.axis;
	case SDL_CONTROLLER_BINDTYPE_HAT:
		return (long)(bind.value.hat.hat * 100 + bind.value.hat.hat_mask);
	}
}

// ----------------------------------------------------------
// Condition / Action / Expression jump table
// ----------------------------------------------------------
// Contains the address inside the extension of the different
// routines that handle the action, conditions and expressions.
// Located at the end of the source for convinience
// Must finish with a 0
//
long (WINAPI * ConditionJumps[])(LPRDATA rdPtr, long param1, long param2) = 
			{ 
			IsConnected,
			SDLEventButtonDown,
			ButtonHeldDown,
			SDLEventButtonUp,
			SDLEventAnyButtonDown,
			AnyButtonHeldDown,
			SDLEventAnyButtonUp,
			IsGameController,
			HasRumble,
			SDLEventDeviceAdded,
			SDLEventDeviceRemoved,
			SDLEventAxisMotion,
			LeftStickUp,
			LeftStickDown,
			LeftStickLeft,
			LeftStickRight,
			RightStickUp,
			RightStickDown,
			RightStickLeft,
			RightStickRight,
			TriggerLeft,
			TriggerRight,
			AxisHold,
			SDLEventJoystickHatMotion,
			SDLEventButtonA,
			SDLEventButtonB,
			SDLEventButtonX,
			SDLEventButtonY,
			SDLEventButtonStart,
			SDLEventButtonBack,
			SDLEventButtonGuide,
			SDLEventButtonLeftStick,
			SDLEventButtonRightStick,
			SDLEventButtonLeftShoulder,
			SDLEventButtonRightShoulder,
			SDLEventDpadUp,
			SDLEventDpadDown,
			SDLEventDpadLeft,
			SDLEventDpadRight,
			HoldButtonA,
			HoldButtonB,
			HoldButtonX,
			HoldButtonY,
			HoldButtonStart,
			HoldButtonBack,
			HoldButtonGuide,
			HoldButtonLeftStick,
			HoldButtonRightStick,
			HoldButtonLeftShoulder,
			HoldButtonRightShoulder,
			HoldDpadUp,
			HoldDpadDown,
			HoldDpadLeft,
			HoldDpadRight,
			DeviceHasLED,
			SDLEventJoystickBallMotion,
			SDLEventTouchpad,
			SDLEventFinger,
			SDLEventTouchpadMotion,
			SDLEventFingerMotion,
			SDLEventBattery,
			HasTriggerRumble,
			HasAccelerator,
			AcceleratorEnabled,
			HasGyroscope,
			GyroscopeEnabled,
			SDLEventRemapped,
			SDLEventAnyRemapped,
			SDLEventSensor,
			IsGameControllerEnabled,
			0
			};
	
short (WINAPI * ActionJumps[])(LPRDATA rdPtr, long param1, long param2) =
			{
			LoadDB,
			Rumble,
			SetLEDColor,
			AddMapping,
			AddDeviceMapping,
			RumbleTriggers,
			SetPlayer,
			SetAccelerometerState,
			SetGyroscopeState,
			PollInput,
			0
			};

long (WINAPI * ExpressionJumps[])(LPRDATA rdPtr, long param) = 
			{     
			NumJoysticks,
			GetAxis,
			GetButton,
			GetHat,
			GetBallX,
			GetBallY,
			NumAxes,
			NumButtons,
			NumHats,
			NumBalls,
			GetDeviceName,
			GetDeviceGUID,
			GetButtonsHeld,
			HeldButton,
			LastButtonPressed,
			LastButtonReleased,
			GetAxisInitial,
			GetBatteryLevel,
			NumTouchpads,
			NumFingers,
			GetFingerState,
			GetFingerX,
			GetFingerY,
			GetFingerPressure,
			JoystickType,
			ControllerType,
			GetPlayer,
			DeviceVendor,
			DeviceProduct,
			DeviceProductVersion,
			DeviceFirmware,
			DeviceSerial,
			GetAcceleratorState,
			GetAcceleratorX,
			GetAcceleratorY,
			GetAcceleratorZ,
			GetGyroscopeState,
			GetGyroscopeX,
			GetGyroscopeY,
			GetGyroscopeZ,
			GetBindStringDevice,
			GetBindStringAxis,
			GetBindStringButton,
			GetBindTypeAxis,
			GetBindValueAxis,
			GetBindTypeButton,
			GetBindValueButton,
			0
			};