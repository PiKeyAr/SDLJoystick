// Object identifier "SDLJ"

#define IDENTIFIER	MAKEID(S,D,L,J)		// REQUIRED: you MUST replace the letters in the MAKEID macro by others
										// and then remove the #pragma message above. If you do not do this, MMF2
										// could confuse your object with another in the event editor.
#include "SDL.h"
#include "GlobalData.h"

// ------------------------------
// DEFINITION OF CONDITIONS CODES
// ------------------------------

// Used in versions before 2.0
#define	CND_ISCONNECTED					0
#define	CND_E_BUTTONDOWN				1
#define	CND_BUTTON_HOLD					2
#define	CND_E_BUTTONUP					3
#define	CND_E_BUTTONDOWN_ANY			4
#define	CND_BUTTON_ANY_HOLD				5
#define	CND_E_BUTTONUP_ANY				6
#define	CND_ISCONTROLLER     			7
#define	CND_HASRUMBLE       			8

// Added in 2.0
// Devices
#define	CND_E_DEVICEADDED				9
#define	CND_E_DEVICEREMOVED				10
// CND_ISCONNECTED already defined
// CND_ISCONTROLLER already defined
// CND_HASRUMBLE already defined

// Axes
#define	CND_E_AXISMOTION				11
#define	CND_LEFTSTICK_UP_HOLD			12
#define	CND_LEFTSTICK_DOWN_HOLD			13
#define	CND_LEFTSTICK_LEFT_HOLD			14
#define	CND_LEFTSTICK_RIGHT_HOLD		15
#define	CND_RIGHTSTICK_UP_HOLD			16
#define	CND_RIGHTSTICK_DOWN_HOLD		17
#define	CND_RIGHTSTICK_LEFT_HOLD		18
#define	CND_RIGHTSTICK_RIGHT_HOLD		19
#define	CND_TRIGGER_LEFT_HOLD			20
#define	CND_TRIGGER_RIGHT_HOLD			21
#define	CND_AXIS_HOLD					22
// Hat
#define	CND_E_JOYHATMOTION				23

// Buttons pressed or released
#define	CND_E_BUTTON_A					24
#define	CND_E_BUTTON_B					25
#define	CND_E_BUTTON_X					26
#define	CND_E_BUTTON_Y					27
#define	CND_E_BUTTON_START				28
#define	CND_E_BUTTON_BACK				29
#define	CND_E_BUTTON_GUIDE				30
#define	CND_E_BUTTON_LEFTSTICK			31
#define	CND_E_BUTTON_RIGHTSTICK			32
#define	CND_E_BUTTON_LEFTSHOULDER		33
#define	CND_E_BUTTON_RIGHTSHOULDER		34
#define	CND_E_DPAD_UP					35
#define	CND_E_DPAD_DOWN					36
#define	CND_E_DPAD_LEFT					37
#define	CND_E_DPAD_RIGHT				38
// CND_E_BUTTONDOWN already defined
// CND_E_BUTTONUP already defined
// CND_E_BUTTONDOWN_ANY already defined
// CND_E_BUTTONUP_ANY already defined

// Buttons held down
#define	CND_BUTTON_A_HOLD				39
#define	CND_BUTTON_B_HOLD				40
#define	CND_BUTTON_X_HOLD				41
#define	CND_BUTTON_Y_HOLD				42
#define	CND_BUTTON_START_HOLD			43
#define	CND_BUTTON_BACK_HOLD			44
#define	CND_BUTTON_GUIDE_HOLD			45
#define	CND_BUTTON_LEFTSTICK_HOLD		46
#define	CND_BUTTON_RIGHTSTICK_HOLD		47
#define	CND_BUTTON_LEFTSHOULDER_HOLD	48
#define	CND_BUTTON_RIGHTSHOULDER_HOLD	49
#define	CND_DPAD_UP_HOLD				50
#define	CND_DPAD_DOWN_HOLD				51
#define	CND_DPAD_LEFT_HOLD				52
#define	CND_DPAD_RIGHT_HOLD				53
// CND_BUTTON_HOLD already defined
// CND_BUTTON_ANY_HOLD already defined

// Introduced in version 2.10

#define	CND_HASLED						54
#define	CND_E_BALLMOTION				55
#define	CND_E_TOUCHPAD					56
#define	CND_E_FINGER					57
#define	CND_E_TOUCHPADMOTION			58
#define	CND_E_FINGERMOTION				59
#define	CND_E_BATTERY					60

// Introduced in version 2.20
#define	CND_HASTRIGGERRUMBLE			61
#define	CND_HASACCEL					62
#define	CND_HASACCELENABLED				63
#define	CND_HASGYRO						64
#define	CND_HASGYROENABLED				65
#define	CND_E_REMAPPED					66
#define	CND_E_REMAPPED_ANY				67
#define	CND_E_SENSOR					68
#define	CND_ISGAMECONTROLLERENABLED		69
#define	CND_LAST						70

// ---------------------------
// DEFINITION OF ACTIONS CODES
// ---------------------------
#define	ACT_LOADDB					0
#define ACT_RUMBLE					1
#define ACT_SETLED					2
#define ACT_SETMAPPING				3
#define ACT_SETMAPPING_DEVICE		4
#define ACT_TRIGGERRUMBLE			5
#define ACT_SETPLAYER				6
#define ACT_SETACCEL				7
#define ACT_SETGYRO 				8
#define ACT_POLL 					9
#define	ACT_LAST					10

// -------------------------------
// DEFINITION OF EXPRESSIONS CODES
// -------------------------------
#define	EXP_NUMJOYS                 0
#define	EXP_GETAXIS                 1
#define	EXP_GETBUTTON               2
#define	EXP_GETHAT                  3
#define	EXP_GETBALLX                4
#define	EXP_GETBALLY                5
#define	EXP_NUMAXES                 6
#define	EXP_NUMBUTTONS              7
#define	EXP_NUMHATS                 8
#define	EXP_NUMBALLS                9
#define EXP_DEVICENAME 				10
#define EXP_DEVICEGUID 				11
#define	EXP_GETBUTTONSHELD	        12
#define	EXP_HELDBUTTON	            13
#define	EXP_LASTPRESSED	            14
#define	EXP_LASTRELEASED	        15
#define	EXP_GETAXISINITIAL	        16
#define	EXP_GETBATTERYLEVEL	        17
#define	EXP_NUMTOUCHPADS	        18
#define	EXP_NUMFINGERS		        19
#define	EXP_GETFINGERSTATE	        20
#define	EXP_GETFINGERX				21
#define	EXP_GETFINGERY				22
#define	EXP_GETFINGERPRESSURE	    23
#define	EXP_JOYTYPE					24
#define	EXP_CONTROLLERTYPE			25
#define	EXP_GETPLAYER				26
#define	EXP_VENDOR					27
#define	EXP_PRODUCT					28
#define	EXP_PRODUCTVERSION			29
#define	EXP_FIRMWARE				30
#define	EXP_SERIAL					31
#define	EXP_GETACCELSTATE			32
#define	EXP_GETACCELX				33
#define	EXP_GETACCELY				34
#define	EXP_GETACCELZ				35
#define	EXP_GETGYROSTATE			36
#define	EXP_GETGYROX				37
#define	EXP_GETGYROY				38
#define	EXP_GETGYROZ				39
#define	EXP_GETBINDSTRING_DEVICE	40
#define	EXP_GETBINDSTRING_AXIS		41
#define	EXP_GETBINDSTRING_BUTTON	42
#define	EXP_GETBINDTYPE_AXIS		43
#define	EXP_GETBINDVALUE_AXIS		44
#define	EXP_GETBINDTYPE_BUTTON		45
#define	EXP_GETBINDVALUE_BUTTON		46
#define	EXP_LAST                    47

// ---------------------
// OBJECT DATA STRUCTURE 
// ---------------------
// Used at edit time and saved in the MFA/CCN/EXE files

typedef struct tagEDATA_V1
{
	// Header - required
	extHeader		eHeader;

	// Object's data
	bool            disableGameController; // Disables the GameController interface
	TCHAR			librarySearchPath[1];  // Removed - SDL2.DLL search path

} EDITDATA;
typedef EDITDATA *			LPEDATA;

// Object versions
#define	KCX_CURRENT_VERSION			1

// --------------------------------
// RUNNING OBJECT DATA STRUCTURE
// --------------------------------
// Used at runtime. Initialize it in the CreateRunObject function.
// Free any allocated memory or object in the DestroyRunObject function.
//
// Note: if you store C++ objects in this structure and do not store 
// them as pointers, you must call yourself their constructor in the
// CreateRunObject function and their destructor in the DestroyRunObject
// function. As the RUNDATA structure is a simple C structure and not a C++ object.

typedef struct tagRDATA
{
	// Main header - required
	headerObject	rHo;					// Header

	// Optional headers - depend on the OEFLAGS value, see documentation and examples for more info
//	rCom			rc;				// Common structure for movements & animations
//	rMvt			rm;				// Movements
//	rSpr			rs;				// Sprite (displayable objects)
	rVal			rv;				// Alterable values

	// Object's runtime data
	long            debugItem;      // Joystick ID to be displayed in debugger
} RUNDATA;
typedef	RUNDATA	*			LPRDATA;

// Size when editing the object under level editor
// -----------------------------------------------
#define	MAX_EDITSIZE			sizeof(EDITDATA)

// Default flags - see documentation for more info
// -------------
#define	OEFLAGS	(OEFLAG_NEVERSLEEP|OEFLAG_RUNBEFOREFADEIN|OEFLAG_VALUES|OEFLAG_SCROLLINGINDEPENDANT|OEFLAG_NEVERKILL)
#define	OEPREFS	0


// If to handle message, specify the priority of the handling procedure
// 0= low, 255= very high. You should use 100 as normal.                                                
// --------------------------------------------------------------------
#define	WINDOWPROC_PRIORITY		100