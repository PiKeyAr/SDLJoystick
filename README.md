# SDL Joystick Object by PkR

SDL Joystick is an extension for Clickteam Fusion 2.5 that uses the [Joystick](https://www.libsdl.org/release/SDL-1.2.15/docs/html/joystick.html) and [GameController](https://wiki.libsdl.org/CategoryGameController) APIs of the [SDL2 library](https://github.com/libsdl-org/SDL/) to interface with game controllers.

You must conform to SDL2 [license terms](https://www.libsdl.org/license.php) in all projects using this extension.

See [the wiki](https://gitlab.com/PiKeyAr/SDLJoystick/-/wikis/home) for more information.